package chatcmds;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

public class clearchat
  implements CommandExecutor
{
  @SuppressWarnings("unused")
private static final String chatObject = null;

  @EventHandler(priority=EventPriority.HIGH)
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
	  if (!(sender instanceof Player)) {
	      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
	      return true;
	    }

    Player p = (Player)sender;

    if ((commandLabel.equalsIgnoreCase("clearchat")) && 
      (p.hasPermission("clearchat.use")))
    {
      if (args.length == 0)
      {
        

        for (int i = 0; i < 500; i++){
        	Bukkit.broadcastMessage("");
        }
        Bukkit.broadcastMessage("§4Endstone§cPvP §4§l» §7The chat has been cleared by §c" + sender.getName() + "§7!");	
      }

    }

    return false;
  }
}