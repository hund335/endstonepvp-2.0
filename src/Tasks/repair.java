package Tasks;


import java.util.Map;

import mc.hund35.main.main;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class repair
  implements Runnable
{
  private Player ply;
  private Item spawnedItem;
  private int animationTask;
  
  public repair(Player ply, Item item, main plugin)
  {
    this.ply = ply;
    this.spawnedItem = item;
    
    this.animationTask = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable()
    {
      public void run()
      {
        repair.this.spawnedItem.getWorld().playEffect(repair.this.spawnedItem.getLocation(), Effect.STEP_SOUND, 145);
        repair.this.spawnedItem.getWorld().playSound(repair.this.spawnedItem.getLocation(), Sound.BLOCK_ANVIL_FALL, 0.5F, (float)Math.random() * 0.9F + 1.2F);
      }
    }, 20L, 10L);
  }
  
  public void run()
  {
    Map<Integer, ItemStack> givenItems = this.ply.getInventory().addItem(new ItemStack[] { this.spawnedItem.getItemStack() });
    for (ItemStack stack : givenItems.values())
    {
      Item item = this.ply.getWorld().dropItem(this.ply.getLocation(), stack);
      item.setItemStack(stack);
    }
    this.spawnedItem.remove();
    Bukkit.getScheduler().cancelTask(this.animationTask);
  }
}