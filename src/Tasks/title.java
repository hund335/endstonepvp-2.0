package Tasks;

import net.minecraft.server.v1_10_R1.EntityPlayer;
import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_10_R1.PacketPlayOutTitle;
import net.minecraft.server.v1_10_R1.PacketPlayOutTitle.EnumTitleAction;
import net.minecraft.server.v1_10_R1.PlayerConnection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class title
{
  public static void broadcastAllTitle(Integer fadeIn, Integer stay, Integer fadeOut, String text)
  {
    for(Player p : Bukkit.getServer().getOnlinePlayers()) {
      playTitle(p, fadeIn, stay, fadeOut, text);
    }
  }
  
  public static void broadcastAllSubTitle(Integer fadeIn, Integer stay, Integer fadeOut, String text)
  {
	  for(Player p : Bukkit.getServer().getOnlinePlayers()) {
      playTitle(p, fadeIn, stay, fadeOut, null, text);
    }
  }
  
  public static void broadcastAllFullTitle(Integer fadeIn, Integer stay, Integer fadeOut, String subtitle, String title)
  {
	  for(Player p : Bukkit.getServer().getOnlinePlayers()) {
      playTitle(p, fadeIn, stay, fadeOut, title, subtitle);
    }
  }
  
  public static void playTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String text)
  {
    playTitle(player, fadeIn, stay, fadeOut, text, null);
  }
  
  public static void playSubTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String text)
  {
    playTitle(player, fadeIn, stay, fadeOut, null, text);
  }
  
  public static void playFullTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle)
  {
    playTitle(player, fadeIn, stay, fadeOut, title, subtitle);
  }
  
  public static void playTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle)
  {
    PlayerConnection connection = ((CraftPlayer)player).getHandle().playerConnection;
    
    PacketPlayOutTitle packetPlayOutTimes = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TIMES, null, fadeIn.intValue(), stay.intValue(), fadeOut.intValue());
    connection.sendPacket(packetPlayOutTimes);
    if (subtitle != null)
    {
      subtitle = subtitle.replaceAll("%player%", player.getDisplayName());
      subtitle = ChatColor.translateAlternateColorCodes('&', subtitle);
      IChatBaseComponent titleSub = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + subtitle + "\"}");
      PacketPlayOutTitle packetPlayOutSubTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, titleSub);
      connection.sendPacket(packetPlayOutSubTitle);
    }
    if (title != null)
    {
      title = title.replaceAll("%player%", player.getDisplayName());
      title = ChatColor.translateAlternateColorCodes('&', title);
      IChatBaseComponent titleMain = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + title + "\"}");
      PacketPlayOutTitle packetPlayOutTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, titleMain);
      connection.sendPacket(packetPlayOutTitle);
    }
  }
  
  /* Error */
  public static void playTabTitle(Player player, String header, String footer)
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull +6 -> 7
    //   4: ldc -98
    //   6: astore_1
    //   7: bipush 38
    //   9: aload_1
    //   10: invokestatic 111	org/bukkit/ChatColor:translateAlternateColorCodes	(CLjava/lang/String;)Ljava/lang/String;
    //   13: astore_1
    //   14: aload_2
    //   15: ifnonnull +6 -> 21
    //   18: ldc -98
    //   20: astore_2
    //   21: bipush 38
    //   23: aload_2
    //   24: invokestatic 111	org/bukkit/ChatColor:translateAlternateColorCodes	(CLjava/lang/String;)Ljava/lang/String;
    //   27: astore_2
    //   28: aload_1
    //   29: ldc 101
    //   31: aload_0
    //   32: invokeinterface 103 1 0
    //   37: invokevirtual 107	java/lang/String:replaceAll	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang