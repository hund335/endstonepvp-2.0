package Tasks;

import mc.hund35.*;
import mc.hund35.main.main;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

@SuppressWarnings("unused")
public class Trade
{
  main plugin;
  Player tradeRequester = null;
  Player tradeAccepter = null;
  Inventory requesterInventory = null;
  Inventory accepterInventory = null;
  ItemStack[] requesterTradeRequestItems = new ItemStack[0];
  ItemStack[] accepterTradeRequestItems = new ItemStack[0];
  boolean countdownInProgress = false;
  boolean tradeAccepted = false;
  boolean requesterReady = false;
  boolean accepterReady = false;
  boolean tradeReady = false;
  boolean cancelled = false;
  int countdown = -1;
  
  public Trade(main plugin, Player tradeRequester, Player tradeAccepter)
  {
    this.plugin = plugin;
    this.tradeRequester = tradeRequester;
    this.tradeAccepter = tradeAccepter;
   
   // if (tradeRequester == tradeRequester) {
    //	tradeRequester.sendMessage("§4Endstone§cPvP §4§l» §cYou cannot trade yourself!");
    	//Trade.this.cancelTrade(true);
    	//return;
    //} 
   
   tradeRequester.sendMessage("§4Endstone§cPvP §4§l» §7Trade request sent to the player §c" + tradeAccepter.getName());
   tradeAccepter.sendMessage("§4Endstone§cPvP §4§l» §7The player §c" + tradeRequester.getName() + " §7has just requested a trade with you. You have 15 seconds to accept");
   startTimeOutCounter();
	
   
    	
    }
  
  
  
  public void openTrade()
  {
    if (!isCancelled()) {
      if ((this.tradeRequester.isOnline()) && (this.tradeAccepter.isOnline()) && (this.tradeRequester.getLocation().distance(this.tradeAccepter.getLocation()) <= 10.0D))
      {
        String invName = "|You|                  |Other|";
        Inventory inv = Bukkit.createInventory(null, 36, invName);
        ItemStack divider = main.getItem(Material.IRON_FENCE, 1, 0, ChatColor.RESET, new String[0]);
        ItemStack declineTrade = main.getItem(Material.WOOL, 1, 14, ChatColor.RED + "Click to Cancel Trade", new String[] { ChatColor.GRAY + "Click this block at any", ChatColor.GRAY + "time during the trade to cancel" });
        ItemStack acceptTrade = main.getItem(Material.WOOL, 1, 5, ChatColor.GREEN + "Click to Ready Trade", new String[] { ChatColor.GRAY + "Once both traders have clicked", ChatColor.GRAY + "this block, the countdown will", ChatColor.GRAY + "begin" });
        ItemStack readyOrNot = main.getItem(Material.INK_SACK, 1, 8, ChatColor.GRAY + "Not Ready", new String[0]);
        inv.setItem(4, divider);
        inv.setItem(13, divider);
        inv.setItem(22, divider);
        inv.setItem(31, divider);
        inv.setItem(27, acceptTrade);
        inv.setItem(28, declineTrade);
        inv.setItem(30, readyOrNot);
        inv.setItem(35, readyOrNot);
        this.requesterInventory = Bukkit.createInventory(null, 36, invName);
        this.requesterInventory.setContents(inv.getContents());
        this.accepterInventory = Bukkit.createInventory(null, 36, invName);
        this.accepterInventory.setContents(inv.getContents());
        this.tradeRequester.openInventory(this.requesterInventory);
        this.tradeAccepter.openInventory(this.accepterInventory);
      }
      else
      {
        cancelTrade(true);
      }
    }
  }
  
  @SuppressWarnings("deprecation")
public void closeTrade()
  {
    ItemStack[] empty = new ItemStack[0];
    if ((this.tradeRequester.isOnline()) && (this.tradeRequester.getOpenInventory() != null) && 
      (this.tradeRequester.getOpenInventory().getTopInventory() != null))
    {
      this.tradeRequester.closeInventory();
      this.tradeRequester.updateInventory();
    }
    if ((this.tradeAccepter.isOnline()) && (this.tradeAccepter.getOpenInventory() != 