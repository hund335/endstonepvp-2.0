package Tasks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class generator<T>
{
  private Random r;
  
  class Possibility
  {
    public T Choice;
    public int Chance;
    public int RangeMax;
    public int RangeMin;
    
    Possibility(T paramT, T paramInt)
    {
      this.Choice = paramInt;
      int i = 0;
      this.Chance = i;
    }

	public Possibility(T paramT, int paramInt) {
		// TODO Auto-generated constructor stub
	}
  }
  
  public generator(Random paramRandom)
  {
    this.r = paramRandom;
  }
  
  public generator()
  {
    this.r = new Random();
  }
  
  private List<generator<T>.Possibility> possibilities = new ArrayList();
private generator<T>.Possibility localObject;
  
  public void addPossibility(T paramT, int paramInt)
  {
    this.possibilities.add(new Possibility(paramT, paramInt));
  }
  
  public T getRandomResult()
  {
    if (this.possibilities.size() == 0) {
      return null;
    }
    int i = 0;
    for (Object localObject = this.possibilities.iterator(); ((Iterator)localObject).hasNext();)
    {
      Possibility localPossibility = (Possibility)((Iterator)localObject).next();
      localPossibility.RangeMin = i;
      localPossibility.RangeMax = (i + localPossibility.Chance);
      i += localPossibility.Chance;
    }
    int j = 1 + this.r.nextInt(i);
    for (Iterator localIterator = this.possibilities.iterator(); localIterator.hasNext();)
    {
      localObject = (Possibility)localIterator.next();
      if ((j <= ((Possibility)localObject).RangeMax) && (j > ((Possibility)localObject).RangeMin)) {
        return (T)((Possibility)localObject).Choice;
      }
    }
    return null;
  }
}
