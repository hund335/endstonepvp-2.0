package Tasks;

import java.util.ArrayList;

import mc.hund35.main.main;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings("unused")
public class TradeUtil
{
  private main plugin;
  @SuppressWarnings({ "unchecked", "rawtypes" })
private ArrayList<Trade> tradeList = new ArrayList();
  
  public TradeUtil(main trading)
  {
    this.plugin = trading;
  }
  
  public ArrayList<Trade> getAllTrades()
  {
    return this.tradeList;
  }
  
  @SuppressWarnings("rawtypes")
public void removeTrade(Trade trade)
  {
    @SuppressWarnings("unchecked")
	ArrayList<Trade> trades = new ArrayList();
    for (Trade t : this.tradeList) {
      if ((!t.getRequester().getName().equalsIgnoreCase(trade.getRequester().getName())) || (!t.getAccepter().getName().equalsIgnoreCase(trade.getAccepter().getName()))) {
        trades.add(t);
      }
    }
    this.tradeList = trades;
  }
  
  public void addTrade(Trade trade)
  {
    removeTrade(trade);
    this.tradeList.add(trade);
  }
  
  public Trade getTradeFromRequester(Player requester)
  {
    Trade trade = null;
    for (Trade t : this.tradeList) {
      if (t.getRequester().getName().equalsIgnoreCase(requester.getName())) {
        trade = t;
      }
    }
    return trade;
  }
  
  public Trade getTradeFromAccepter(Player accepter)
  {
    Trade trade = null;
    for (Trade t : this.tradeList) {
      if (t.getAccepter().getName().equalsIgnoreCase(accepter.getName())) {
        trade = t;
      }
    }
    return trade;
  }
  
  @SuppressWarnings({ "unchecked", "rawtypes" })
public Inventory setItemsLeft(Inventory inventory, ItemStack[] itemsArray)
  {
    Inventory inv = inventory;
    ArrayList<ItemStack> items = new ArrayList();
    ItemStack[] arrayOfItemStack;
    int j = (arrayOfItemStack = itemsArray).length;
    for (int i = 0; i < j; i++)
    {
      ItemStack item = arrayOfItemStack[i];
      items.add(item);
    }
    int slot = 0;
    while (items.size() > 0) {
      if ((slot != 3) && (slot != 12) && (slot != 21))
      {
        inv.setItem(slot, (ItemStack)items.get(0));
        items.remove(items.get(0));
        slot++;
      }
      else
      {
        inv.setItem(slot, (ItemStack)items.get(0));
        items.remove(items.get(0));
        slot += 6;
      }
    }
    return inv;
  }
  
  @SuppressWarnings("unchecked")
public Inventory setItemsRight(Inventory inventory, ItemStack[] itemsArray)
  {
    Inventory inv = inventory;
    @SuppressWarnings("rawtypes")
	ArrayList<ItemStack> items = new ArrayList();
    ItemStack[] arrayOfItemStack;
    int j = (arrayOfItemStack = itemsArray).length;
    for (int i = 0; i < j; i++)
    {
      ItemStack item = arrayOfItemStack[i];
      items.add(item);
    }
    int slot = 5;
    while (items.size() > 0) {
      if ((slot != 8) && (slot != 17) && (slot != 26))
      {
        inv.setItem(slot, (ItemStack)items.get(0));
        items.remove(items.get(0));
        slot++;
      }
      else
      {
        inv.setItem(slot, (ItemStack)items.get(0));
        items.remove(items.get(0));
        slot += 6;
      }
    }
    return inv;
  }
  
  @SuppressWarnings({ "rawtypes", "unchecked" })
public ItemStack[] getItemsRequester(Player p)
  {
    ArrayList<ItemStack> items = new ArrayList();
    if (getTradeFromRequester(p) != null)
    {
      Trade trade = getTradeFromRequester(p);
      if (trade.hasTradeWindowOpen(p))
      {
        Inventory inv = p.getOpenInventory().getTopInventory();
        for (int slot = 0; slot < 4; slot++) {
          if (inv.getItem(slot) != null) {
            items.add(inv.getItem(slot));
          }
        }
        for (int slot = 9; slot < 13; slot++) {
          if (inv.getItem(slot) != null) {
            items.add(inv.getItem(slot));
          }
        }
        for (int slot = 18; slot < 22; slot++) {
          if (inv.getItem(slot) != null) {
            items.add(inv.getItem(slot));
          }
        }
      }
    }
    return (ItemStack[])items.toArray(new ItemStack[ite