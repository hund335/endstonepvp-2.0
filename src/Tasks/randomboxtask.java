package Tasks;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import mc.hund35.main.main;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.util.Vector;

import configs.randomboxconfig;
import features.randombox;

@SuppressWarnings("unused")
public class randomboxtask
  implements Runnable
{
  private main plugin;
  private Player ply;
  private randomboxconfig config;
  private randombox box;
  private int rollItemTask;
  private Item currentItem;
  private Block block;
  
  public randomboxtask(main plugin, Player ply, final Block box, randombox feature)
  {
    this.plugin = plugin;
    this.ply = ply;
    this.box = feature;
    this.config = feature.getConfig();
    this.block = box;
    
    
    
    this.rollItemTask = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable()
    {
      public void run()
      {
        if (randomboxtask.this.currentItem != null)
        {
          randomboxtask.this.currentItem.remove();
          randomboxtask.this.currentItem = null;
        }
        Random rnd = new Random();
        ItemStack item;
        boolean rename = true;
        String displayName;
        switch (rnd.nextInt(8))
        {
        case 0: 
        default: 
          item = new ItemStack(Material.DIAMOND_SWORD, 1);
          displayName = "&eSword";
          ItemMeta gapple = item.getItemMeta();
          gapple.setDisplayName("§eSword");
          item.setItemMeta(gapple);
          break;
        case 1: 
          item = new ItemStack(Material.BOW, 1);
          displayName = "&eWooden Bow";
          ItemMeta gapple2 = item.getItemMeta();
          gapple2.setDisplayName("§eWooden Bow");
          item.setItemMeta(gapple2);
          break;
        case 2: 
          item = new ItemStack(Material.DIAMOND_HELMET, 1);
          displayName = "&eHelmet";
          ItemMeta gapple3 = item.getItemMeta();
          gapple3.setDisplayName("§eHelmet");
          item.setItemMeta(gapple3);
          break;
        case 3: 
          item = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
          displayName = "&eChestplate";
          ItemMeta gapple4 = item.getItemMeta();
          gapple4.setDisplayName("§eChestplate");
          item.setItemMeta(gapple4);
          break;
        case 4: 
          item = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
          displayName = "&eLeggings";
          ItemMeta gapple5 = item.getItemMeta();
          gapple5.setDisplayName("§eLeggings");
          item.setItemMeta(gapple5);
          break;
        case 5: 
          item = new ItemStack(Material.DIAMOND_BOOTS, 1);
          displayName = "&eBoots";
          ItemMeta gapple6 = item.getItemMeta();
          gapple6.setDisplayName("§eBoots");
          item.setItemMeta(gapple6);
          break;
        case 6: 
          Potion potion = new Potion(PotionType.values()[rnd.nextInt(PotionType.values().length)]);
          if (rnd.nextInt(2) == 1) {
            potion.setSplash(true);
          }
          item = potion.toItemStack(1);
          break;
        case 7: 
          item = new ItemStack(Material.GOLDEN_APPLE, 1, (short)1);
          ItemMeta gapple7 = item.getItemMeta();
          gapple7.setDisplayName("§5God Apple");
          item.setItemMeta(gapple7);
        }
        randomboxtask.this.currentItem = box.getWorld().dropItem(box.getLocation().add(0.5D, 1.2D, 0.5D), item);
        randomboxtask.this.currentItem.setVelocity(new Vector(0, 0, 0));
        randomboxtask.this.cur