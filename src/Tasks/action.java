package Tasks;

import net.minecraft.server.v1_10_R1.EntityPlayer;
import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import net.minecraft.server.v1_10_R1.PlayerConnection;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class action
{
  public static void playAction(Player player, String message)
  {
    CraftPlayer p = (CraftPlayer)player;
    IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message + "\"}");
    
    PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte)2);
    p.getHandle().playerConnection.sendPacket(ppoc);
  }
  
  public static void broadcastAllAction(String message)
  {
    for (Player p : Bukkit.getServer().getOnlinePlayers()) {
      playAction(p, message);
    }
  }
}