package signs;

      
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffectType;

@SuppressWarnings("unused")
public class godarmor
  implements Listener
{
	 public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	
	 @EventHandler
	  public void onSignChange(SignChangeEvent e)
	  {
	    if (e.getLine(0).equalsIgnoreCase("godarmour"))  {
	    	e.setLine(0, "§l");
	        e.setLine(1, "§lGod Armour");
	        e.setLine(2, "Cooldown: 25 mins");
	    }
	  }
  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent e) {
    if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
    if ((e.getClickedBlock().getState() instanceof Sign)) {
      Sign s = (Sign)e.getClickedBlock().getState();
      if (s.getLine(1).equalsIgnoreCase("§lGod Armour")) {
    	  Player player = e.getPlayer();
    	  
    	  Player p = (Player)player; 	  
            
    	  
    	  int cooldownTime1 = 25; // Get number of seconds from wherever you want
          if(cooldowns.containsKey(p.getName())) {
              long minutes = ((cooldowns.get(p.getName())/60000)+cooldownTime1) - (System.currentTimeMillis()/60000);
              if(minutes>0) {
                  // Still cooling down
                  p.sendMessage("§4Endstone§cPvP §4§l» §cYou need to wait §4§o" + minutes + " minutes§c.");
                  return;
    	  
              }
         
          }
          // No cooldown found or cooldown has expired, save new cooldown
          cooldowns.put(p.getName(), System.currentTimeMillis());  
          p.sendMessage("§4Endstone§cPvP §4§l» §7You have successfully claimed the god armour.");
          PlayerInventory pi = p.getInventory(); 
          ItemStack helm = new ItemStack(Material.DIAMOND_HELMET, 1);
          helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
          helm.addUnsafeEnchantment(Enchantment.DURABILITY, 4);
          ItemMeta helmmeta = helm.getItemMeta();
          helmmeta.setDisplayName(ChatColor.YELLOW + "God Helmet");
          List<String> lore = new ArrayList<String>();
          lore.add(ChatColor.GOLD + "Soulbound");
          helmmeta.setLore(lore);
          helm.setItemMeta(helmmeta);
          pi.addItem(helm);                   
          //Chestplate
          ItemStack cp = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
          cp.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
          cp.addUnsafeEnchantment(Enchantment.DURABILITY, 4);
          ItemMeta cpmeta = cp.getItemMeta();
          cpmeta.setDisplayName(ChatColor.YELLOW + "God Chestplate");
          List<String> lore1 = new ArrayList<String>();
          lore1.add(ChatColor.GOLD + "Soulbound");
          cpmeta.setLore(lore1);
          cp.setItemMeta(cpmeta);
          pi.addItem(cp);        
          //Leggings
          ItemStack leggings = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
          leggings.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
          leggings.addUnsafeEnchantment(Enchantment.DURABILITY, 4);
          ItemMeta leggingsmeta = leggings.getItemMeta();
          leggingsmeta.setDisplayName(ChatColor.YELLOW + "God Leggings");
          List<String> lore11 = new ArrayList<String>();
          lore11.add(ChatColor.GOLD + "Soulbound");
          leggingsmeta.setLore(lore11);
          leggings.setItemMeta(leggingsmeta);
          pi.addItem(leggings);  
          //Boots               
          ItemStack boots = new ItemStack(Material.DIAMOND_BOOTS, 1);
          boots.addUns