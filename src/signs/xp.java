package signs;

      
import java.util.HashMap;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class xp
  implements Listener
{
	
	
	 public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	
	
	 @EventHandler
	  public void onSignChange(SignChangeEvent e)
	  {
	    if (e.getLine(0).equalsIgnoreCase("freexp"))  {
	    	e.setLine(0, "§l");
	        e.setLine(1, "§lFree XP");
	    }
	  }
  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent e) {
    if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
    if ((e.getClickedBlock().getState() instanceof Sign)) {
      Sign s = (Sign)e.getClickedBlock().getState();
      if (s.getLine(1).equalsIgnoreCase("§lFree XP")) {
    	  Player player = e.getPlayer();
    	  
    	  Player p = (Player)player;

    	  
    	  
    	  
    	  int cooldownTime1 = 60; // Get number of seconds from wherever you want
          if(cooldowns.containsKey(p.getName())) {
              long secondsLeft = ((cooldowns.get(p.getName())/1000)+cooldownTime1) - (System.currentTimeMillis()/1000);
              if(secondsLeft>0) {
                  // Still cooling down
                  p.sendMessage("§4Endstone§cPvP §4§l» §cYou need to wait §4§o" + secondsLeft + " seconds§c.");
                  return;
              }
          }
          // No cooldown found or cooldown has expired, save new cooldown
          cooldowns.put(p.getName(), System.currentTimeMillis());
    	  
    	  
    	  p.giveExpLevels(30);
    	  p.sendMessage("§4Endstone§cPvP §4§l» §7You received 30 levels..");

        		    
                        		    	
    	 	   
    	     	          p.updateInventory();
    	  }
      }
    }
  
   
        	  
     
  }
