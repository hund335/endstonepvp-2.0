package signs;

      
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffectType;

@SuppressWarnings("unused")
public class godweapons
  implements Listener
{
	 public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	
	 @EventHandler
	  public void onSignChange(SignChangeEvent e)
	  {
	    if (e.getLine(0).equalsIgnoreCase("godweapons"))  {
	    	e.setLine(0, "§l");
	        e.setLine(1, "§lGod Weapons");
	        e.setLine(2, "Cooldown: 25 mins");
	    }
	  }
  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent e) {
    if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
    if ((e.getClickedBlock().getState() instanceof Sign)) {
      Sign s = (Sign)e.getClickedBlock().getState();
      if (s.getLine(1).equalsIgnoreCase("§lGod Weapons")) {
    	  Player player = e.getPlayer();
    	  
    	  Player p = (Player)player; 	  
            
    	  
    	  int cooldownTime1 = 25; // Get number of seconds from wherever you want
          if(cooldowns.containsKey(p.getName())) {
              long minutes = ((cooldowns.get(p.getName())/60000)+cooldownTime1) - (System.currentTimeMillis()/60000);
              if(minutes>0) {
                  // Still cooling down
                  p.sendMessage("§4Endstone§cPvP §4§l» §cYou need to wait §4§o" + minutes + " minutes§c.");
                  return;
    	  
              }
         
          }
          // No cooldown found or cooldown has expired, save new cooldown
          cooldowns.put(p.getName(), System.currentTimeMillis());   	
          p.sendMessage("§4Endstone§cPvP §4§l» §7You have successfully claimed the god weapons.");
          PlayerInventory pi = p.getInventory(); 
          ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);
          sword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 6);
          sword.addEnchantment(Enchantment.DURABILITY, 1);
          ItemMeta swordmeta = sword.getItemMeta();
          swordmeta.setDisplayName(ChatColor.YELLOW + "God Sword");
          List<String> lore = new ArrayList<String>();
          lore.add(ChatColor.GOLD + "Soulbound");
          swordmeta.setLore(lore);
          sword.setItemMeta(swordmeta);
          pi.addItem(sword);  
          
          ItemStack bow = new ItemStack(Material.BOW, 1);
          bow.addEnchantment(Enchantment.ARROW_DAMAGE, 5);
          bow.addUnsafeEnchantment(Enchantment.ARROW_FIRE, 2);
          ItemMeta bowmeta = bow.getItemMeta();
          bowmeta.setDisplayName(ChatColor.YELLOW + "God Bow");
          List<String> lore1 = new ArrayList<String>();
          lore1.add(ChatColor.GOLD + "Soulbound");
          bowmeta.setLore(lore1);
          bow.setItemMeta(bowmeta);
          pi.addItem(bow); 
    	     	          p.updateInventory();
    	  }
      }
    }
  
   
        	  
     
  }