package signs;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class premium
  implements Listener
{
	 @EventHandler
	  public void onSignChange(SignChangeEvent e)
	  {
	    if (e.getLine(0).equalsIgnoreCase("/premium"))  {
	    	e.setLine(0, "§l");
	        e.setLine(1, "§l/Premium");
	    }
	  }
  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent e) {
    if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
    if ((e.getClickedBlock().getState() instanceof Sign)) {
      Sign s = (Sign)e.getClickedBlock().getState();
      if (s.getLine(1).equalsIgnoreCase("§l/Premium")) {
    	  Player player = e.getPlayer();
    	  
    	  Player p = (Player)player;
    	  p.sendMessage(ChatColor.AQUA + "§6§m§l---------------------------------------------");
          p.sendMessage(ChatColor.GOLD + "§7Premiums can only take 1 kit every hour.");
          p.sendMessage("");
          p.sendMessage(ChatColor.DARK_RED + "/premium 1" + ChatColor.RED + " Sword kit -" + ChatColor.GRAY + " Sharp 4");
          p.sendMessage(ChatColor.DARK_RED + "/premium 2" + ChatColor.RED + " Bow kit -" + ChatColor.GRAY + " Power 4");
          p.sendMessage(ChatColor.DARK_RED + "/premium 3" + ChatColor.RED + " Armour kit -" + ChatColor.GRAY + " Prot 4");
          p.sendMessage(ChatColor.DARK_RED + "/premium 4" + ChatColor.RED + " God Apple kit -" + ChatColor.GRAY + " 1 x God Apple");
          p.sendMessage(ChatColor.DARK_RED + "/premium 5" + ChatColor.RED + " Star kit -" + ChatColor.GRAY + " 1 x Star");
          p.sendMessage(ChatColor.AQUA + "§6§m§l---------------------------------------------");
            
    	  }
      }
    }

  
}