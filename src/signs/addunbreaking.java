package signs;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class addunbreaking
  implements Listener
{
	
	 public HashMap<String, Long> cooldowns = new HashMap<String, Long>();

	 @EventHandler
	  public void onSignChange(SignChangeEvent e)
	  {
	    if (e.getLine(0).equalsIgnoreCase("addunbreaking"))  {
	    	e.setLine(0, "§l");
	        e.setLine(1, "§lAdd");
	      e.setLine(2, "Unbreaking");
	    }
	  }
  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent e) {
    if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
    if ((e.getClickedBlock().getState() instanceof Sign)) {
      Sign s = (Sign)e.getClickedBlock().getState();
      if (s.getLine(2).equalsIgnoreCase("Unbreaking")) {
    	  
Player player = e.getPlayer();
    	  
    	  Player p = (Player)player;  
      
 
    	  
    	  
    	  int cooldownTime1 = 60; // Get number of seconds from wherever you want
          if(cooldowns.containsKey(p.getName())) {
              long minutes = ((cooldowns.get(p.getName())/60000)+cooldownTime1) - (System.currentTimeMillis()/60000);
              if(minutes>0) {
                  // Still cooling down
                  p.sendMessage("§4Endstone§cPvP §4§l» §cYou need to wait §4§o" + minutes + " minutes§c.");
                  return;
    	  
              }
         
          }
          // No cooldown found or cooldown has expired, save new cooldown
          cooldowns.put(p.getName(), System.currentTimeMillis());


ItemStack is = p.getItemInHand();
          
    	  if (player.getItemInHand().getType().equals(Material.AIR) || player.getItemInHand().getType().equals(Material.ACACIA_STAIRS) || player.getItemInHand().getType().equals(Material.ACTIVATOR_RAIL) || player.getItemInHand().getType().equals(Material.ANVIL) || player.getItemInHand().getType().equals(Material.APPLE) || player.getItemInHand().getType().equals(Material.ARROW) || player.getItemInHand().getType().equals(Material.BAKED_POTATO) || player.getItemInHand().getType().equals(Material.BEACON) || player.getItemInHand().getType().equals(Material.BED) || player.getItemInHand().getType().equals(Material.BED_BLOCK) || player.getItemInHand().getType().equals(Material.BIRCH_WOOD_STAIRS) || player.getItemInHand().getType().equals(Material.BLAZE_POWDER) || player.getItemInHand().getType().equals(Material.BLAZE_ROD) || player.getItemInHand().getType().equals(Material.BOAT) || player.getItemInHand().getType().equals(Material.BONE) || player.getItemInHand().getType().equals(Material.BOOK) || player.getItemInHand().getType().equals(Material.BOOK_AND_QUILL) || player.getItemInHand().getType().equals(Material.BOOKSHELF) || player.getItemInHand().getType().equals(Material.BOWL) || player.getItemInHand().getType().equals(Material.BREAD) || player.getItemInHand().getType().equals(Material.BREWING_STAND) || player.getItemInHand().getType().equals(Material.BREWING_STAND_ITEM) || player.getItemInHand().getType().equals(Material.BRICK) || player.getItemInHand().getType().equals(Material.BRICK_STAIRS) || player.getItemInHand().getType().equals(Material.BROWN_MUSHROOM) || player.getItemInHand().getType().equals(Material.BUCKET) || player.getItemInHand().getType().equals(Material.BURNING_FURNACE) || player.getItemInHand().getType().equals(Material.CACTUS) || player.getItemInHand().getType().equals(Material.CAKE) || player.getItemInHand().getType().equals(Material.CAKE_BLOCK) || player.getItemInHand().getType().equals(Material.CARPET) || player.getItemInHand().getType().equals(Material.CARROT) || player.getItemInHand().getType().equals(Material.CARROT_ITEM) || player.getItemInHand().getType().equals(Material.CARROT_STICK) || player.getItemInHand().getType().equals(Material.CAULDRON) || p