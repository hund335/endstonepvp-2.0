package signs;

      
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class poisonswordshop
  implements Listener
{
	 @EventHandler
	  public void onSignChange(SignChangeEvent e)
	  {
	    if (e.getLine(0).equalsIgnoreCase("poisonswordshop"))  {
	    	e.setLine(0, "§l");
	        e.setLine(1, "§lBuy");
	        e.setLine(2, "A poison sword");
	    }
	  }
  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent e) {
    if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
    if ((e.getClickedBlock().getState() instanceof Sign)) {
      Sign s = (Sign)e.getClickedBlock().getState();
      if (s.getLine(2).equalsIgnoreCase("A poison sword")) {
    	  Player player = e.getPlayer();
    	  
    	  Player p = (Player)player; 	  
    	  if (!p.getInventory().contains(Material.NETHER_STAR, 50))
 	   	 {	    
 			  p.sendMessage("§4Endstone§cPvP §4§l» §cYou do not have 50 x stars.");
 			  return;
 	   	 }
 		p.getInventory().removeItem(new ItemStack[] {
 				  new ItemStack(Material.NETHER_STAR, 50) });
 		 p.sendMessage("§4Endstone§cPvP §4§l» §7You bought a §cPoison Sword§7!");
 		PlayerInventory pi = p.getInventory();
        ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);
        sword.addEnchantment(Enchantment.DAMAGE_ALL, 3);
        ItemMeta swordmeta = sword.getItemMeta();
        swordmeta.setDisplayName(ChatColor.DARK_PURPLE + "Poison Sword");
        List<String> lore = new ArrayList<String>();
        lore.add("§7Poison Effect I");
        lore.add("§6Non-sharable");
        swordmeta.setLore(lore);
        sword.setItemMeta(swordmeta);
        pi.addItem(sword);             		    	
    	 	   
    	     	          p.updateInventory();
    	  }
      }
    }
  
   
        	  
     
  }