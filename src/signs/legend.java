package signs;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class legend
  implements Listener
{
	 @EventHandler
	  public void onSignChange(SignChangeEvent e)
	  {
	    if (e.getLine(0).equalsIgnoreCase("/legend"))  {
	    	e.setLine(0, "§l");
	        e.setLine(1, "§l/Legend");
	    }
	  }
  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent e) {
    if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
    if ((e.getClickedBlock().getState() instanceof Sign)) {
      Sign s = (Sign)e.getClickedBlock().getState();
      if (s.getLine(1).equalsIgnoreCase("§l/Legend")) {
    	  Player player = e.getPlayer();
    	  
    	  Player p = (Player)player;
    	  p.sendMessage(ChatColor.AQUA + "§6§m§l---------------------------------------------");
          p.sendMessage(ChatColor.GOLD + "§7Legends can only take 1 kit every hour.");
          p.sendMessage("");
          p.sendMessage(ChatColor.DARK_RED + "/legend 1" + ChatColor.RED + " Sword kit -" + ChatColor.GRAY + " Sharp 4 Fire 1");
          p.sendMessage(ChatColor.DARK_RED + "/legend 2" + ChatColor.RED + " Bow kit -" + ChatColor.GRAY + " Power 4 Flame 1");
          p.sendMessage(ChatColor.DARK_RED + "/legend 3" + ChatColor.RED + " Armour kit -" + ChatColor.GRAY + " Prot 4 FireProt 1");
          p.sendMessage(ChatColor.DARK_RED + "/legend 4" + ChatColor.RED + " Potion kit -" + ChatColor.GRAY + " 1 x Speed & 1 x Strength ");
          p.sendMessage(ChatColor.DARK_RED + "/legend 5" + ChatColor.RED + " God Apple kit -" + ChatColor.GRAY + " 2 x God Apple");
          p.sendMessage(ChatColor.DARK_RED + "/legend 6" + ChatColor.RED + " Axe kit -" + ChatColor.GRAY + " Sharp 4 Poison 1");
          p.sendMessage(ChatColor.DARK_RED + "/legend 7" + ChatColor.RED + " Star kit -" + ChatColor.GRAY + " 1 x Star");
          p.sendMessage(ChatColor.AQUA + "§6§m§l---------------------------------------------");
            
    	  }
      }
    }

  
}