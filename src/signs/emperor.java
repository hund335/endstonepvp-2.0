package signs;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class emperor
  implements Listener
{
	 @EventHandler
	  public void onSignChange(SignChangeEvent e)
	  {
	    if (e.getLine(0).equalsIgnoreCase("/emperor"))  {
	    	e.setLine(0, "§l");
	        e.setLine(1, "§l/Emperor");
	    }
	  }
  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent e) {
    if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
    if ((e.getClickedBlock().getState() instanceof Sign)) {
      Sign s = (Sign)e.getClickedBlock().getState();
      if (s.getLine(1).equalsIgnoreCase("§l/Emperor")) {
    	  Player player = e.getPlayer();
    	  
    	  Player p = (Player)player;
    	  p.sendMessage(ChatColor.AQUA + "§6§m§l---------------------------------------------");
          p.sendMessage(ChatColor.GOLD + "§7Emperors can only take 1 kit every hour.");
          p.sendMessage("");
          p.sendMessage(ChatColor.DARK_RED + "/emperor 1" + ChatColor.RED + " Sword kit -" + ChatColor.GRAY + " Sharp 5 Fire 1");
          p.sendMessage(ChatColor.DARK_RED + "/emperor 2" + ChatColor.RED + " Bow kit -" + ChatColor.GRAY + " Power 4 Poison 1");
          p.sendMessage(ChatColor.DARK_RED + "/emperor 3" + ChatColor.RED + " Armour kit -" + ChatColor.GRAY + " Prot 4 FireProt 2 Unbreaking 2");
          p.sendMessage(ChatColor.DARK_RED + "/emperor 4" + ChatColor.RED + " Potion kit -" + ChatColor.GRAY + " 1 x Speed & 1 x Strength ");
          p.sendMessage(ChatColor.DARK_RED + "/emperor 5" + ChatColor.RED + " God Apple kit -" + ChatColor.GRAY + " 2 x God Apple");
          p.sendMessage(ChatColor.DARK_RED + "/emperor 6" + ChatColor.RED + " Axe kit -" + ChatColor.GRAY + " Sharp 5 Poison 1");
          p.sendMessage(ChatColor.DARK_RED + "/emperor 7" + ChatColor.RED + " Star kit -" + ChatColor.GRAY + " 1 x Star");
          p.sendMessage(ChatColor.DARK_RED + "/emperor 8" + ChatColor.RED + " Splash Potion kit -" + ChatColor.GRAY + " 3 x Instant Damage & 1 x Slowness ");
          p.sendMessage(ChatColor.AQUA + "§6§m§l---------------------------------------------");
            
    	  }
      }
    }

  
}