package signs;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import guis.parkour;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings("unused")
public class parkoursign
  implements Listener
{
	
	 public HashMap<String, Long> cooldowns = new HashMap<String, Long>();

	 @EventHandler
	  public void onSignChange(SignChangeEvent e)
	  {
	    if (e.getLine(0).equalsIgnoreCase("parkourreward"))  {
	    	e.setLine(0, "§l");
	        e.setLine(1, "§lReward");
	    }
	  }
  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent e) {
    if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
    if ((e.getClickedBlock().getState() instanceof Sign)) {
      Sign s = (Sign)e.getClickedBlock().getState();
      if (s.getLine(1).equalsIgnoreCase("§lReward")) {
    	  
Player player = e.getPlayer();
    	  
    	  Player p = (Player)player;  
      
 
    	  
    	  
    	  int cooldownTime1 = 24; // Get number of seconds from wherever you want
          if(cooldowns.containsKey(p.getName())) {
              long minutes = ((cooldowns.get(p.getName())/3600000)+cooldownTime1) - (System.currentTimeMillis()/3600000);
              if(minutes>0) {
                  // Still cooling down
                  p.sendMessage("§4Endstone§cPvP §4§l» §cYou need to wait §4§o" + minutes + " hours§c.");
                  return;
    	  
              }
         
          }
          // No cooldown found or cooldown has expired, save new cooldown
          cooldowns.put(p.getName(), System.currentTimeMillis());

         p.openInventory(parkour.parkour);
      
    	  } 
        		    
      
    
  
           
    }
    }
}
    
  
