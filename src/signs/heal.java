package signs;

      
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffectType;

public class heal
  implements Listener
{
	 @EventHandler
	  public void onSignChange(SignChangeEvent e)
	  {
	    if (e.getLine(0).equalsIgnoreCase("heal"))  {
	    	e.setLine(0, "§l");
	        e.setLine(1, "§lHeal");
	    }
	  }
  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent e) {
    if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
    if ((e.getClickedBlock().getState() instanceof Sign)) {
      Sign s = (Sign)e.getClickedBlock().getState();
      if (s.getLine(1).equalsIgnoreCase("§lHeal")) {
    	  Player player = e.getPlayer();
    	  
    	  Player p = (Player)player; 	  
          p.sendMessage("§4Endstone§cPvP §4§l» §7You have been healed.");
          p.setHealth(20);
          p.setFireTicks(0);
          p.removePotionEffect(PotionEffectType.BLINDNESS);
          p.removePotionEffect(PotionEffectType.CONFUSION);
          p.removePotionEffect(PotionEffectType.SLOW);
          p.removePotionEffect(PotionEffectType.HUNGER);
          p.removePotionEffect(PotionEffectType.POISON);
          p.removePotionEffect(PotionEffectType.WEAKNESS);
          p.removePotionEffect(PotionEffectType.WITHER);	    
                        		    	
    	 	   
    	     	          p.updateInventory();
    	  }
      }
    }
  
   
        	  
     
  }