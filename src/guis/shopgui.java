package guis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

@SuppressWarnings("unused")
public class shopgui implements Listener {
	
	

private static ItemStack star;

private static ItemStack dhelmet;

private static ItemStack dchestplate;

private static ItemStack dleggings;

private static ItemStack dboots;

private static ItemStack battlesword;

private static ItemStack sword;

private static ItemStack battlebow;

private static ItemStack bow;

private static ItemStack instanthealth;

private static ItemStack fireres;

private static ItemStack regen;

private static ItemStack speed;

private static ItemStack strength;

private static ItemStack godapple;

private static ItemStack godapples;


public static Inventory shop = Bukkit.createInventory(null, 45, "Shop");

static
{

	star = star(ChatColor.RED + "§c§lThe items costs stars.");
	
	shop.setItem(0, star);
	
	dhelmet = dhelmet(ChatColor.YELLOW + "Helmet");
	
	shop.setItem(18, dhelmet);
	
	dchestplate = dchestplate(ChatColor.YELLOW + "Chestplate");
	
	shop.setItem(19, dchestplate);
	
    dleggings = dleggings(ChatColor.YELLOW + "Leggings");
	
    shop.setItem(20, dleggings);
	
	dboots = dboots(ChatColor.YELLOW + "Boots");
	
	shop.setItem(21, dboots);
	
	battlesword = battlesword(ChatColor.YELLOW + "Battle Sword");
	
	shop.setItem(25, battlesword);
	 
    sword = sword(ChatColor.YELLOW + "Sword");
	
	shop.setItem(26, sword);
	
    battlebow = battlebow(ChatColor.YELLOW + "Battle Bow");
	
	shop.setItem(34, battlebow);
	
    bow = bow(ChatColor.YELLOW + "Wooden Bow");
	
	shop.setItem(35, bow);   
	
    instanthealth = instanthealth(ChatColor.DARK_PURPLE + "Instant Health Potion");
	
	shop.setItem(2, instanthealth);  
	
    fireres = fireres(ChatColor.DARK_PURPLE + "Fire Resistance Potion");
	
	shop.setItem(3, fireres);  
	
    regen = regen(ChatColor.DARK_PURPLE + "Regeneration II Potion");
	
	shop.setItem(4, regen);
	
    speed = speed(ChatColor.DARK_PURPLE + "Speed Potion");
	
	shop.setItem(5, speed);
	
    strength = strength(ChatColor.DARK_PURPLE + "Strength Potion");
	
	shop.setItem(6, strength);
	
    godapple = godapple(ChatColor.DARK_PURPLE + "1x God Apple");
	
	shop.setItem(7, godapple);
	
	godapples = godapples(ChatColor.DARK_PURPLE + "5x God Apples");
		
    shop.setItem(8, godapples);
	
	
   
}

private static ItemStack star(String name){
	  ItemStack i = new ItemStack(new ItemStack(Material.NETHER_STAR));
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		i.setItemMeta(im);
		return i;
	}
private static ItemStack dhelmet(String name){
	ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_HELMET));
	i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		 List<String> lore = new ArrayList<String>();
         lore.add("");
         lore.add("§c§lCosts §4§l2 §c§lstars");
         lore.add("§c§lClick to buy this");
         im.setLore(lore);
		i.setItemMeta(im);
		return i;
	}
private static ItemStack dchestplate(String name){
	ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_CHESTPLATE));
	i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		List<String> lore = new ArrayList<String>();
        lore.add("");
        lore.add("§c§lCosts §4§l2 §c§lstars");
        lore.add("§c§lClick to buy this");
        im.setLore(lore);
		i.setItemMeta(im);
		return i;
	}
private static ItemStack dleggings(String name){
	ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_LEGGINGS));
	i.addEnchantment(Enchantment.PRO