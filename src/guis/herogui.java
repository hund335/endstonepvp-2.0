package guis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import admincmds.vanish;
import packages.master;

@SuppressWarnings("unused")
public class herogui
  implements Listener
{
	
	public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
  private static ItemStack sword;
  private static ItemStack bow;
  private static ItemStack armour;
  private static ItemStack godapple;
  private static ItemStack star;
  private static ItemStack potion;
  private static ItemStack axe;
  public static Inventory herogui = Bukkit.createInventory(null, 18, "§b§lHero §7- §8Choose a kit!");
  
  static
  {
    sword = sword(ChatColor.YELLOW + "§e§lSword kit");
    
    herogui.setItem(1, sword);
    
bow = bow(ChatColor.YELLOW + "§e§lBow kit");
    
    herogui.setItem(3, bow);
    
armour = armour(ChatColor.YELLOW + "§e§lArmour kit");
    
    herogui.setItem(5, armour);
    
godapple = godapple(ChatColor.YELLOW + "§e§lGod apple kit");
    
    herogui.setItem(7, godapple);
    
star = star(ChatColor.YELLOW + "§e§lStar kit");
    
    herogui.setItem(11, star);
    
    
potion = potion(ChatColor.YELLOW + "§e§lPotion kit");
    
    herogui.setItem(13, potion);
    
axe = axe(ChatColor.YELLOW + "§e§lAxe kit");
    
    herogui.setItem(15, axe);
    
  }
  
  @SuppressWarnings({ "unchecked", "rawtypes" })
private static ItemStack sword(String name)
  {
    ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_SWORD, 1));
    i.addEnchantment(Enchantment.DAMAGE_ALL, 4);
    i.addEnchantment(Enchantment.FIRE_ASPECT, 1);
    ItemMeta im = i.getItemMeta();
    im.setDisplayName(name);
    List<String> lore = new ArrayList();
    lore.add("§6§m§l-----------------");
    lore.add("§eThis will give you");
    lore.add("§ea sword.");
    im.setLore(lore);
    i.setItemMeta(im);
    return i;
  }
  
  @SuppressWarnings({ "unchecked", "rawtypes" })
private static ItemStack bow(String name)
  {
    ItemStack i = new ItemStack(new ItemStack(Material.BOW, 1));
    i.addEnchantment(Enchantment.ARROW_DAMAGE, 3);
    i.addEnchantment(Enchantment.ARROW_FIRE, 1);
    ItemMeta im = i.getItemMeta();
    im.setDisplayName(name);
    List<String> lore = new ArrayList();
    lore.add("§6§m§l-----------------");
    lore.add("§eThis will give you");
    lore.add("§ea bow.");
    im.setLore(lore);
    i.setItemMeta(im);
    return i;
  }
  
  @SuppressWarnings({ "rawtypes", "unchecked" })
private static ItemStack armour(String name)
  {
    ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_CHESTPLATE, 1));
    i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
    i.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
    ItemMeta im = i.getItemMeta();
    im.setDisplayName(name);
    List<String> lore = new ArrayList();
    lore.add("§6§m§l-----------------");
    lore.add("§eThis will give you");
    lore.add("§ea full set of armour.");
    im.setLore(lore);
    i.setItemMeta(im);
    return i;
  }
  
  @SuppressWarnings({ "rawtypes", "unchecked" })
private static ItemStack godapple(String name)
  {
    ItemStack i = new ItemStack(new ItemStack(Material.GOLDEN_APPLE, 1,(short)1));
    ItemMeta im = i.getItemMeta();
    im.setDisplayName(name);
    List<String> lore = new ArrayList();
    lore.add("§6§m§l-----------------");
    lore.add("§eThis will give you");
    lore.add("§e1 god apple.");
    im.setLore(lore);
    i.setItemMeta(im);
    return i;
  }
  @SuppressWarnings({ "rawtype