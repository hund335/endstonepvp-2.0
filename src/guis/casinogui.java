package guis;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

@SuppressWarnings("unused")
public class casinogui
  implements Listener
{
  private static ItemStack star1;
  public static Inventory casino = Bukkit.createInventory(null, 9, "Casino");
  
  static
  {
    star1 = star1(ChatColor.DARK_PURPLE + "Try your luck!");
    
    casino.setItem(4, star1);
  }
  
  @SuppressWarnings({ "unchecked", "rawtypes" })
private static ItemStack star1(String name)
  {
    ItemStack i = new ItemStack(new ItemStack(Material.NETHER_STAR, 1));
    ItemMeta im = i.getItemMeta();
    im.setDisplayName(name);
    List<String> lore = new ArrayList();
    lore.add("");
    lore.add("§d§lCosts §5§l5 §d§lstars");
    lore.add("§d§lClick to rate your stars.");
    im.setLore(lore);
    i.setItemMeta(im);
    return i;
  }
  
  @EventHandler
  public void casino(InventoryClickEvent e)
  {
    if (!e.getInventory().getName().equalsIgnoreCase(casino.getName())) {
      return;
    }
    Inventory inventory = e.getInventory();
    if (inventory.getTitle().equalsIgnoreCase("Casino"))
    {
      e.setCancelled(true);
      Random rand = new Random();
      int number = rand.nextInt(100) + 1;
      if (e.getCurrentItem().getItemMeta() == null) {
        return;
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().contains("§5Try your luck!"))
      {
        if (!e.getWhoClicked().getInventory().contains(Material.NETHER_STAR, 5))
        {
          e.getWhoClicked().sendMessage("§4Endstone§cPvP §4§l» §cYou do not have enough stars to rate.");
          return;
        }
        e.setCancelled(true);
        e.getWhoClicked().getInventory().removeItem(new ItemStack[] {
          new ItemStack(Material.NETHER_STAR, 5) });
        e.getWhoClicked().sendMessage("§4Endstone§cPvP §4§l» §c5x stars has been removed from your inventory!");
        if (number <= 20)
        {
          e.setCancelled(true);
          Bukkit.broadcastMessage("§4Endstone§cPvP §4§l» §c" + e.getWhoClicked().getName() + " §7won §c10x stars §7in our casino!");
          PlayerInventory pi = e.getWhoClicked().getInventory();
          ItemStack card = new ItemStack(Material.NETHER_STAR, 10);
          ItemMeta cardmeta = card.getItemMeta();
          card.setItemMeta(cardmeta);
          pi.addItem(new ItemStack[] { card });
          e.getWhoClicked().closeInventory();
        }
      }
    }
  }
}

