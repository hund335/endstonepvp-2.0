package features;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class rename
implements CommandExecutor 

{
	public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player)) {
      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
      return true;
    }
    Player player = (Player)sender;
    if (commandLabel.equalsIgnoreCase("rename")) {
      if (!player.hasPermission("rename.use")) {
        player.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
        return true;
      }

      if (args.length == 0) {
        player.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /rename <ID>");
        return true;
      }
      if (args.length == 1) {
        ItemStack is = player.getItemInHand();
       
        
        if (player.getItemInHand().getType().equals(Material.AIR) || player.getItemInHand().getType().equals(Material.ACACIA_STAIRS) || player.getItemInHand().getType().equals(Material.ACTIVATOR_RAIL) || player.getItemInHand().getType().equals(Material.ANVIL) || player.getItemInHand().getType().equals(Material.APPLE) || player.getItemInHand().getType().equals(Material.ARROW) || player.getItemInHand().getType().equals(Material.BAKED_POTATO) || player.getItemInHand().getType().equals(Material.BEACON) || player.getItemInHand().getType().equals(Material.BED) || player.getItemInHand().getType().equals(Material.BED_BLOCK) || player.getItemInHand().getType().equals(Material.BIRCH_WOOD_STAIRS) || player.getItemInHand().getType().equals(Material.BLAZE_POWDER) || player.getItemInHand().getType().equals(Material.BLAZE_ROD) || player.getItemInHand().getType().equals(Material.BOAT) || player.getItemInHand().getType().equals(Material.BONE) || player.getItemInHand().getType().equals(Material.BOOK) || player.getItemInHand().getType().equals(Material.BOOK_AND_QUILL) || player.getItemInHand().getType().equals(Material.BOOKSHELF) || player.getItemInHand().getType().equals(Material.BOWL) || player.getItemInHand().getType().equals(Material.BREAD) || player.getItemInHand().getType().equals(Material.BREWING_STAND) || player.getItemInHand().getType().equals(Material.BREWING_STAND_ITEM) || player.getItemInHand().getType().equals(Material.BRICK) || player.getItemInHand().getType().equals(Material.BRICK_STAIRS) || player.getItemInHand().getType().equals(Material.BROWN_MUSHROOM) || player.getItemInHand().getType().equals(Material.BUCKET) || player.getItemInHand().getType().equals(Material.BURNING_FURNACE) || player.getItemInHand().getType().equals(Material.CACTUS) || player.getItemInHand().getType().equals(Material.CAKE) || player.getItemInHand().getType().equals(Material.CAKE_BLOCK) || player.getItemInHand().getType().equals(Material.CARPET) || player.getItemInHand().getType().equals(Material.CARROT) || player.getItemInHand().getType().equals(Material.CARROT_ITEM) || player.getItemInHand().getType().equals(Material.CARROT_STICK) || player.getItemInHand().getType().equals(Material.CAULDRON) || player.getItemInHand().getType().equals(Material.CAULDRON_ITEM) || player.getItemInHand().getType().equals(Material.CHAINMAIL_BOOTS) || player.getItemInHand().getType().equals(Material.CHAINMAIL_CHESTPLATE) || player.getItemInHand().getType().equals(Material.CHAINMAIL_HELMET) || player.getItemInHand().getType().equals(Material.CHAINMAIL_LEGGINGS) || player.getItemInHand().getType().equals(Material.CHEST) || player.getItemInHand().getType().equals(Material.CLAY) || player.getItemInHand().getType().equals(Material.CLAY_BALL) || player.getItemInHand().getType().equals(Material.CLAY_BRICK) || player.getItemInHand().getType().equals(Material.COAL) || player.getItemInHand().g