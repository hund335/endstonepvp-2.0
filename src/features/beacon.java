package features;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import Tasks.title;

@SuppressWarnings("unused")
public class beacon
implements Listener{

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, Long> cooldowns = new HashMap();

	
	 
		 @EventHandler
			public void Beacon(PlayerInteractEvent event) {
			  Player player = event.getPlayer();
			  if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
			  if (!event.getClickedBlock().getType().equals(Material.BEACON)) return;
			  event.setCancelled(true);

			  
			  
			  Player p = (Player)player;
			

			  
		    int cooldownTime = 10;  
			  if (this.cooldowns.containsKey(player.getName())) {
			      long minutes = ((Long)this.cooldowns.get(player.getName())).longValue() / 60000 + cooldownTime - System.currentTimeMillis() / 60000;				  
			      if (minutes > 0)
			      {
			    
			    	  player.sendMessage("§4Endstone§cPvP §4§l» §cYou need to wait §4§o" + minutes + " minutes §cfor take the kit again.");
			    	  
			    	  event.setCancelled(true);
			    	  
			  

			        return;
			      }
			    }

			    //this.cooldowns.put(player.getName(), Long.valueOf(System.currentTimeMillis()));
			  
				
		     
			 
			  
			

			 
			 	          
			    	          Random rand = new Random();
			            int number = rand.nextInt(100) + 1;
			    	          
			         Random rand1 = new Random();
			         int number1 = rand1.nextInt(100) + 1;
			    	          
			         Random rand2 = new Random();
			          int number2 = rand2.nextInt(100) + 1;
			    	          
			    	          
			        Random rand3 = new Random();
			    	            int number3 = rand3.nextInt(100) + 1;
			    	          
			         
			    	         
				  	    	          if(number <= 30)
				  	    	       {
			    	        	 
			    	       
				  	    	        	this.cooldowns.put(player.getName(), Long.valueOf(System.currentTimeMillis()));
				  	    	        	player.sendMessage("§4Endstone§cPvP §4§l» §7You got the §cWeakest §7kit!.");
				  	    	          title.playFullTitle(player, 20, 50, 20, "§4Beacon collected!","§7You got the §cWeakest §7kit!");
				  	    	          PlayerInventory pi = player.getInventory();   
				  	    	          
				  	    	          //sword
				  	    	          ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);
				  	    	          sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
				  	    	          ItemMeta swordmeta = sword.getItemMeta();
				  	    	          swordmeta.setDisplayName(ChatColor.YELLOW + "Sword");
				  	    	          List<String> lore = new ArrayList<String>();
				  	    	          swordmeta.setLore(lore);
				  	    	          sword.setItemMeta(swordmeta);
				  	    	          pi.addItem(sword);  
				  	    	       
				  	    	         //bow
				  	    	          ItemStack bow = new ItemStack(Material.BOW, 1);
				  	    	          bow.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
				  	    	          ItemMeta bowmeta = bow.getItemMeta();
				  	    	          bowmeta.setDisplayName(ChatColor.YELLOW + "Wooden Bow");
				  	    	          List<String> lore2 = new ArrayList<String>();
				  	    	          bowmeta.setLore(lore2);
				  	    	          bow.setItemMeta(bowmeta);
				  	    	          pi.addItem(bow);    
				  	    	          
				  	    	          //Helmet
				  	    	          ItemStack helm = new ItemStack(Material.DIAMOND_HELMET, 1);
				  	    	          helm.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
				 