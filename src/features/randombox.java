package features;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import managers.pay;
import mc.hund35.main.main;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import Tasks.randomboxtask;
import configs.randomboxconfig;

public class randombox
  implements Listener
{
  private Map<Player, Block> confirming;
  public List<Block> inUse;
  private main plugin;
  private randomboxconfig config;
  
  @SuppressWarnings({ "unchecked", "rawtypes" })
public randombox(main plugin, randomboxconfig config)
  {
    this.plugin = plugin;
    this.inUse = new ArrayList();
    this.confirming = new HashMap();
    this.config = config;
  }
  
  public randomboxconfig getConfig()
  {
    return this.config;
  }
  
  @SuppressWarnings("deprecation")
@EventHandler
  public void onRandomBoxClick(PlayerInteractEvent event)
  {
    if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
      return;
    }
    if (event.getClickedBlock() == null) {
      return;
    }
    if (event.getClickedBlock().getType() != Material.PISTON_BASE) {
      return;
    }
    Block block = event.getClickedBlock();
    final Player ply = event.getPlayer();
    
    event.setCancelled(true);
    if (!ply.getWorld().getName().equalsIgnoreCase(this.config.getWorld())) {
      return;
    }
    if (ply.getGameMode() != GameMode.ADVENTURE)
    {
      ply.sendMessage(this.config.getMessage("illegal-gamemode"));
      return;
    }
    if (this.inUse.contains(block))
    {
      ply.sendMessage(this.config.getMessage("already-in-use"));
      return;
    }  
    if (this.config.isUsingConfirm())
    {
      if (this.confirming.containsKey(ply)) {
        return;
      }
      this.confirming.put(ply, block);
      ply.sendMessage(this.config.getMessage("confirm"));
      new BukkitRunnable()
      {
        public void run()
        {
          if (randombox.this.confirming.containsKey(ply))
          {
            randombox.this.confirming.remove(ply);
            ply.sendMessage(randombox.this.config.getMessage("timeout"));
          }
        }
      }.runTaskLater(this.plugin, 20L * this.config.getConfirmationTimeout());
    }
    else
    {
      if (!pay.pay(ply, Material.getMaterial(this.config.getCurrency()), this.config.getPrice()))
      {
        ply.sendMessage(this.config.getMessage("insufficient-funds"));
        return;
      }
      startRandomItemGeneration(ply, block);
    }
  }
  
  @SuppressWarnings("deprecation")
@EventHandler(ignoreCancelled=true)
  public void onConfirm(AsyncPlayerChatEvent event)
  {
    if (!event.getMessage().equalsIgnoreCase("y")) {
      return;
    }
    if (!this.confirming.containsKey(event.getPlayer())) {
      return;
    }
    for (Map.Entry<Player, Block> confEntry : this.confirming.entrySet())
    {
      Player ply = (Player)confEntry.getKey();
      Block block = (Block)confEntry.getValue();
      if (block == this.confirming.get(event.getPlayer()))
      {
        if (ply == event.getPlayer())
        {
          if (!pay.pay(ply, Material.getMaterial(this.config.getCurrency()), this.config.getPrice())) {
            ply.sendMessage(this.config.getMessage("insufficient-funds"));
          } else {
            startRandomItemGeneration(event.getPlayer(), block);
          }
        }
        else {
          ply.sendMessage(this.config.getMessage("ninjaed"));
        }
        this.confirming.remove(ply);
      }
    }
    event.setCancelled(true);
  }
  
  private void startRandomItemGeneration(Player ply, Block block)
  {
    ply.sendMessage(this.config.getMessage("accepted"));
    this.inUse.add(block);
    this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, new randomboxtask(this.plugin, ply, block, this), 60L