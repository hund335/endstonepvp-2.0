package features;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class names
implements CommandExecutor {

	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		
		  {
			    if (!(sender instanceof Player)) {
			      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
			      return true;
			    }
		
	    Player p = (Player)sender;
	
	    if (commandLabel.equalsIgnoreCase("names")) {
	      if (p.hasPermission("rename.list")) {

	        if (args.length == 0) {
	          p.sendMessage("§6§m§l---------------------------------------------");
	          p.sendMessage(ChatColor.GRAY + "Here you see all available names");
	          p.sendMessage(ChatColor.DARK_RED + "");
	          p.sendMessage(ChatColor.DARK_RED + "1" + ChatColor.GOLD + " *" + ChatColor.RED + " Diamond Blade");
	          p.sendMessage(ChatColor.DARK_RED + "2" + ChatColor.GOLD + " *" + ChatColor.RED + " Slayer");
	          p.sendMessage(ChatColor.DARK_RED + "3" + ChatColor.GOLD + " *" + ChatColor.RED + " Burning Shadow");
	          p.sendMessage(ChatColor.DARK_RED + "4" + ChatColor.GOLD + " *" + ChatColor.RED + " Thunder Bow");
	          p.sendMessage(ChatColor.DARK_RED + "5" + ChatColor.GOLD + " *" + ChatColor.RED + " Battle Slayer");
	          p.sendMessage(ChatColor.DARK_RED + "");
	          p.sendMessage("§6§m§l---------------------------------------------");
	         
	        } else {
	        	p.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
	        }
	
	      }
	    }
		return false;
		  }
		  }
}
