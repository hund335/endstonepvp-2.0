package features;

import guis.casinogui;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

@SuppressWarnings("unused")
public class casino
  implements Listener
{
  @EventHandler
  public void Casino(PlayerInteractEvent event)
  {
    Player player = event.getPlayer();
    if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
      return;
    }
    if (!event.getClickedBlock().getType().equals(Material.BURNING_FURNACE)) {
      return;
    }
    player.openInventory(casinogui.casino);
    event.setCancelled(true);
  }
}
