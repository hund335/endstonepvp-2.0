package features;
 
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
 

@SuppressWarnings("unused")
public class soulbound
implements CommandExecutor, Listener
{
  
  
  @SuppressWarnings({ "unchecked" })
public boolean onCommand(CommandSender sender, Command command, String commandlabel, String[] args)
  {
	  if (!(sender instanceof Player)) {
	      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
	      return true;
	    }
	  
    Player p = (Player)sender;
 
    if ((p.getItemInHand() == null) || (p.getItemInHand().getType() == Material.AIR))
    {
      p.sendMessage("§4Endstone§cPvP §4§l» §cYou need to have a item in your hand");
      return false;
    }
 
    if (commandlabel.equalsIgnoreCase("Soulbound"))
    {
      if (!p.hasPermission("Soulbound.use"))
      {
        p.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
        return true;
      }
      ItemStack is = p.getItemInHand();
      ItemMeta im = is.getItemMeta();
      @SuppressWarnings("rawtypes")
    ArrayList lore = new ArrayList();
      if (im.hasLore())
      {
        List<String> oldLore = im.getLore();
        if (oldLore.contains(ChatColor.GOLD + "Soulbound"))
        {
          p.sendMessage("§4Endstone§cPvP §4§l» §cYou already have soulbound on your item");
          return false;
        }
        for (String s : oldLore) {
          lore.add(s);
        }
      }
      lore.add(ChatColor.GOLD + "Soulbound");
      im.setLore(lore);
      is.setItemMeta(im);
      p.sendMessage("§4Endstone§cPvP §4§l» §7You have added soulbound on your item");
      p.updateInventory();
      return true;
    }
 
    if ((p.getItemInHand() == null) || (p.getItemInHand().getType() == Material.AIR))
    {
        p.sendMessage("§4Endstone§cPvP §4§l» §cYou need to have a item in your hand");
      return false;
    }
    return false;
  }
 
 /*
  @EventHandler
  public void onPlayerDeath(PlayerDeathEvent e) {
      ArrayList<ItemStack> remove = new ArrayList<>();
    for (ItemStack i : e.getDrops())
    {
      ItemMeta itemMeta = i.getItemMeta();
      if (itemMeta.hasLore())
      {
        List<String> lore = itemMeta.getLore();
        for (String s : lore) {
          System.out.println(s);
        }
        if (lore.contains(ChatColor.GOLD + "Soulbound")) {
          remove.add(i);
        }
      }
    }
      
    for (ItemStack is : remove) 
    e.getDrops().remove(is);
    }
  */
  
  @EventHandler
  public void onItemDrop(PlayerDropItemEvent event)
  {
    event.getPlayer();
 
    ItemStack is = event.getItemDrop().getItemStack();
    ItemMeta im = is.getItemMeta();
    if (im.hasLore())
    {
      List<String> lore = im.getLore();
      if (lore.contains(ChatColor.GOLD + "Soulbound"))
        event.setCancelled(true);
    }
  }
 
  @EventHandler
  public void onInventoryClick(InventoryClickEvent ev)
  {
    ItemStack clicked = ev.getCurrentItem();
    Inventory inventory = ev.getInventory();
    
    
    
    if (inventory.getTitle().equalsIgnoreCase("container.enderchest") || inventory.getTitle().equalsIgnoreCase("§8§lEnder Chest") || inventory.getTitle().equalsIgnoreCase("container.chest") || inventory.getTitle().equalsIgnoreCase("container.hopper") || inventory.getTitle().equalsIgnoreCase("container.dispenser") || inventory.getTitle().equalsIgnoreCase("container.dropper") || inventory.getTitle().equalsIgnoreCase("container.furnace") || inventory.getTitle().equalsIgnoreCase("container.brewing") || i