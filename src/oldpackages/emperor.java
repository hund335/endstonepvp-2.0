package oldpackages;
 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;
 
public class emperor
implements CommandExecutor
    {
      String prefix = ChatColor.GRAY + "[" + ChatColor.DARK_GRAY + "EndstonePvP" + ChatColor.GRAY + "]";
      
      
      public final Logger logger = Logger.getLogger("Minecraft");
 
      @SuppressWarnings({ "rawtypes", "unchecked" })
    public Map<String, String> oto = new HashMap();
      public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
      
      public void onDisable()
      {
        

      }
 
      public void onEnable()
      {
        
      }
       
        public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
                return true;
        }
           final Player p = (Player)sender;
            if (commandLabel.equalsIgnoreCase("emperor")){
                if(p.hasPermission("emperor.show")){
                    if (args.length == 0) {
                        p.sendMessage(ChatColor.AQUA + "§6§m§l---------------------------------------------");
                        p.sendMessage(ChatColor.GOLD + "§7Emperors can only take 1 kit every hour.");
                        p.sendMessage("");
                        p.sendMessage(ChatColor.DARK_RED + "/emperor 1" + ChatColor.RED + " Sword kit -" + ChatColor.GRAY + " Sharp 5 Fire 2");
                        p.sendMessage(ChatColor.DARK_RED + "/emperor 2" + ChatColor.RED + " Bow kit -" + ChatColor.GRAY + " Power 4 Poison 1");
                        p.sendMessage(ChatColor.DARK_RED + "/emperor 3" + ChatColor.RED + " Armour kit -" + ChatColor.GRAY + " Prot 5 Unbreaking 2");
                        p.sendMessage(ChatColor.DARK_RED + "/emperor 4" + ChatColor.RED + " Potion kit -" + ChatColor.GRAY + " 1 x Speed & 1 x Strength ");
                        p.sendMessage(ChatColor.DARK_RED + "/emperor 5" + ChatColor.RED + " God Apple kit -" + ChatColor.GRAY + " 2 x God Apple");
                        p.sendMessage(ChatColor.DARK_RED + "/emperor 6" + ChatColor.RED + " Axe kit -" + ChatColor.GRAY + " Sharp 5 Poison 1");
                        p.sendMessage(ChatColor.DARK_RED + "/emperor 7" + ChatColor.RED + " Star kit -" + ChatColor.GRAY + " 1 x Star");
                        p.sendMessage(ChatColor.DARK_RED + "/emperor 8" + ChatColor.RED + " Splash Potion kit -" + ChatColor.GRAY + " 3 x Instant Damage & 1 x Slowness ");
                        p.sendMessage(ChatColor.AQUA + "§6§m§l---------------------------------------------");
                        }
                    
                  
                } else {
                    p.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
                    
                }
        
    } else {
        p.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
        
    }
 
 
         
        
            if ((args.length == 1) &&
                    (args[0].equalsIgnoreCase("1")) &&
                    (p.hasPermission("emperor.1"))) {
                     int cooldownTime1 = 60; // Get number of seconds from wherever you want
                     if(cooldowns.containsKey(sender.getName())) {
                         long secondsLeft = ((cooldowns.get(sender.getName())/60000)+cooldownTime1) - (System.currentTimeMillis()/60000);
                         if(secondsLeft>0) {