package hashtags;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;



@SuppressWarnings("unused")
public class hand
  implements Listener
{

	ChatColor currentColor = ChatColor.WHITE;
	  final String fullText = "[\"\",{\"text\":\"[startText]\"}, [replaceItem] ,{\"text\":\"[AfterText]\"}]";

@SuppressWarnings("deprecation")
@EventHandler
public void messageSent1(AsyncPlayerChatEvent event) {
String[] msg = event.getMessage().split(" ");
Player player = event.getPlayer();


//if(event.getMessage().toLowerCase().contains("#hand")){
	//ItemStack item = player.getItemInHand();
	//ItemMeta meta = item.getItemMeta();
//event.setMessage(event.getMessage().replaceAll("#hand", "§6[§5" +meta + "§6]§7"));
	 
	  //event.setCancelled(true);
	if ((event.getMessage().contains("#hand")) && 
		      (event.getPlayer().getItemInHand() != null) && (event.getPlayer().getItemInHand().getType() != Material.AIR))
		    {
		      String newText = "";
		      ItemStack hand = event.getPlayer().getItemInHand();
		      String item = "";
		      
		      String hoverClose = "}";
		      String valueClose = "}\"";
		      String tagClose = "}";
		      String displayClose = "";
		      String finalClose = "}";
		      if (hand.hasItemMeta())
		      {
		        if (hand.getItemMeta().hasDisplayName()) {
		          item = "{ text:\"[" + hand.getItemMeta().getDisplayName() + ChatColor.WHITE + "]\", " + "hoverEvent:{action:show_item, value:\"{id:minecraft:" + hand.getType().toString().toLowerCase() + ",tag:{";
		        } else {
		          item = "{ text:\"[" + formatMaterialName(hand.getType()) + "]\", " + "hoverEvent:{action:show_item, value:\"{id:minecraft:" + hand.getType().toString().toLowerCase() + ",tag:{";
		        }
		        if (hand.getItemMeta().hasEnchants())
		        {
		          item = item + "ench:[{";
		          for (Enchantment ench : hand.getItemMeta().getEnchants().keySet()) {
		            item = item + "id:" + ench.getId() + ",lvl:" + hand.getItemMeta().getEnchants().get(ench) + "},";
		          }
		          item = item + "],";
		        }
		        if ((hand.getItemMeta().hasDisplayName()) || (hand.getItemMeta().hasLore()))
		        {
		          item = item + "display:{";
		          displayClose = "}";
		        }
		        if (hand.getItemMeta().hasDisplayName()) {
		          item = item + "Name:" + rainbow(hand.getItemMeta().getDisplayName(), true) + ",";
		        }
		        if (hand.getItemMeta().hasLore())
		        {
		          item = item + " Lore:[";
		          for (String loreLine : hand.getItemMeta().getLore()) {
		            item = item + "\\\"" + loreLine + "\\\",";
		          }
		          item = item.substring(0, item.lastIndexOf(','));
		          item = item + "]";
		        }
		        item = item + displayClose + tagClose + valueClose + hoverClose + finalClose;
		      }
		      else
		      {
		        item = "{ text:\"[" + formatMaterialName(hand.getType()) + "]\", " + "hoverEvent:{action:show_item, value:\"{id:minecraft:" + hand.getType().toString().toLowerCase() + "}\"}}";
		      }
		      String beforeText = event.getPlayer().getDisplayName() + ChatColor.WHITE + ": " + event.getMessage().substring(0, event.getMessage().indexOf("[item]"));
		      beforeText = rainbow(beforeText, true);
		      String afterText = event.getMessage().substring(event.getMessage().indexOf("]") + 1);
		      afterText = rainbow(afterText, false);
		      newText = newText + "[\"\",{\"text\":\"[startText]\"}, [replaceItem] ,{\"text\":\"[AfterText]\"}]".rep