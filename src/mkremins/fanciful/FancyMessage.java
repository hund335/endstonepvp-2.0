package mkremins.fanciful;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import static mkremins.fanciful.TextualComponent.rawText;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonWriter;

import net.amoebaman.util.ArrayWrapper;
import net.amoebaman.util.Reflection;

import org.bukkit.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.Statistic.Type;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Represents a formattable message. Such messages can use elements such as colors, formatting codes, hover and click data, and other features provided by the vanilla Minecraft <a href="http://minecraft.gamepedia.com/Tellraw#Raw_JSON_Text">JSON message formatter</a>.
 * This class allows plugins to emulate the functionality of the vanilla Minecraft <a href="http://minecraft.gamepedia.com/Commands#tellraw">tellraw command</a>.
 * <p>
 * This class follows the builder pattern, allowing for method chaining.
 * It is set up such that invocations of property-setting methods will affect the current editing component,
 * and a call to {@link #then()} or {@link #then(Object)} will append a new editing component to the end of the message,
 * optionally initializing it with text. Further property-setting method calls will affect that editing component.
 * </p>
 */
public class FancyMessage implements JsonRepresentedObject, Cloneable, Iterable<MessagePart>, ConfigurationSerializable {

	static{
		ConfigurationSerialization.registerClass(FancyMessage.class);
	}

	private List<MessagePart> messageParts;
	private String jsonString;
	private boolean dirty;

	private static Constructor<?> nmsPacketPlayOutChatConstructor;

        @Override
	public FancyMessage clone() throws CloneNotSupportedException{
		FancyMessage instance = (FancyMessage)super.clone();
		instance.messageParts = new ArrayList<MessagePart>(messageParts.size());
		for(int i = 0; i < messageParts.size(); i++){
			instance.messageParts.add(i, messageParts.get(i).clone());
		}
		instance.dirty = false;
		instance.jsonString = null;
		return instance;
	}

	/**
	 * Creates a JSON message with text.
	 * @param firstPartText The existing text in the message.
	 */
	public FancyMessage(final String firstPartText) {
		this(rawText(firstPartText));
	}

	public FancyMessage(final TextualComponent firstPartText) {
		messageParts = new ArrayList<MessagePart>();
		messageParts.add(new MessagePart(firstPartText));
		jsonString = null;
		dirty = false;

		if(nmsPacketPlayOutChatConstructor == null){
			try {
				nmsPacketPlayOutChatConstructor = Reflection.getNMSClass("PacketPlayOutChat").getDeclaredConstructor(Reflection.getNMSClass("IChatBaseComponent"));
				nmsPacketPlayOutChatConstructor.setAccessible(true);
			} catch (NoSuchMethodException e) {
				Bukkit.getLogger().log(Level.SEVERE, "Could not find Minecraft method or constructor.", e);
			} catch (SecurityException e) {
				Bukkit.getLogger().log(Level.WARNING, "Could not access constructor.", e);
			}
		}
	}

	/**
	 * Creates a JSON message without text.
	 */
	public FancyMessage() {
		this((TextualComponent)null);
	}

	/**
	 * Sets the text of the current editing component to a value.
	 * @param text The new text of the current editing component.
	 * @return This builder instance.
	 */
	pu