package configs;

import java.util.ArrayList;
import java.util.List;

import mc.hund35.main.main;

public class randomboxconfig
  extends config
{
  List<Integer> rewards;
  
  public randomboxconfig(main plugin, String name)
  {
    super(plugin, name);
    generateRewards();
  }
  
  @SuppressWarnings({ "unchecked", "rawtypes" })
public void generateRewards()
  {
    this.rewards = new ArrayList();
    for (int i = 0; i < getSwordChance(); i++) {
      this.rewards.add(Integer.valueOf(0));
    }
    for (int i = 0; i < getBowChance(); i++) {
      this.rewards.add(Integer.valueOf(1));
    }
    for (int i = 0; i < getArmourChance(); i++) {
      this.rewards.add(Integer.valueOf(2));
    }
    for (int i = 0; i < getPotionChance(); i++) {
      this.rewards.add(Integer.valueOf(3));
    }
    for (int i = 0; i < getGodAppleChance(); i++) {
      this.rewards.add(Integer.valueOf(4));
    }
  }
  
  public void restoreDefaults()
  {
    this.config.options().header("How you want RandomBox to work\nsword-chance, bow-chance, armour-chance, potion-chance and godapple-chance represent the chances of receiving respecitve item types. ATTENTION: If the percentages do not add up to 100, the feature will be disabled\nconfirmation-timeout is how long (in seconds) the RandomBox waits for the player's confirmation\ncurrency is the itemID of the item you want to use as currency\nprice is how much of the currency it costs to use the RandomBox\nworld is the world in which RandomBox should be effective\nuse-confirm defines whether players should confirm their purchase. If set to false, the RandomBox won't ask for confirmation.");
    
    this.config.set("sword-chance", Integer.valueOf(40));
    this.config.set("bow-chance", Integer.valueOf(30));
    this.config.set("armour-chance", Integer.valueOf(15));
    this.config.set("potion-chance", Integer.valueOf(10));
    this.config.set("godapple-chance", Integer.valueOf(5));
    this.config.set("confirmation-timeout", Integer.valueOf(10));
    this.config.set("currency", Integer.valueOf(265));
    this.config.set("price", Integer.valueOf(64));
    this.config.set("world", "world");
    this.config.set("use-confirm", Boolean.valueOf(false));
    
    setMessage("illegal-gamemode", "&4Endstone&cPvP &4&l» &cYou can only use RandomBox while in Survival!");
    setMessage("already-in-use", "&4Endstone&cPvP &4&l» &cThis RandomBox is already in use.");
    setMessage("confirm", "&4Endstone&cPvP &4&l» &cAre you sure you want to pay &464 iron ingots &7for a random item? Type 'Y' to confirm.");
    setMessage("timeout", "&4Endstone&cPvP &4&l» &cYour RandomBox request has timed out!");
    setMessage("insufficient-funds", "&4Endstone&cPvP &4&l» &cYou do not have enough iron ingots.");
    setMessage("ninjaed", "&4Endstone&cPvP &4&l» &cYour RandomBox request is no longer valid as the RandomBox is in use by someone else.");
    setMessage("accepted", "&4Endstone&cPvP &4&l» &7Have a random item!");
    
    saveConfig();
  }
  
  public int getSwordChance()
  {
    return this.config.getInt("sword-chance", 40);
  }
  
  public int getBowChance()
  {
    return this.config.getInt("bow-chance", 30);
  }
  
  public int getArmourChance()
  {
    return this.config.getInt("armour-chance", 20);
  }
  
  public int getPotionChance()
  {
    return this.config.getInt("potion-chance", 10);
  }
  
  public int getGodAppleChance()
  {
    return this.config.getInt("godapple-chance", 5);
  }
  
  public int getConfirmationTimeout()
  {
    return this.config.getInt("confirmation-timeout", 10);
  }
  
  public int getCurrency()
  {
    return this.config.getInt("currency", 265);
  }
  
  public int getPrice()
  {
    return this.config.getInt("price", 64);
  }
  
  public String getWorld()
  {
    return this.config.getString("world", "world");
  }
  
  public boolean isUsingConfirm()
  {
    return this.config.getBoolean("use-confirm", false);
  }
  
  public List<Integer> getItemIntList()
  {
    return this.rewards;
  }
}
