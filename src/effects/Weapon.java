package effects;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Weapon
  implements CommandExecutor
{
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player)) {
      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
      return true;
    }
    Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("Weapon")) {
      if (!p.hasPermission("weapon.use")) {
        p.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
        return true;
      }

      p.sendMessage("§6§m§l---------------------------------------------");
      p.sendMessage(ChatColor.DARK_RED + "");
      p.sendMessage(ChatColor.RED + "Use" + ChatColor.DARK_RED + " /setpoison" + ChatColor.RED + " for give your weapon poison");
      p.sendMessage(ChatColor.RED + "Use" + ChatColor.DARK_RED + " /setblind" + ChatColor.RED + " for give your weapon blindness");
      p.sendMessage(ChatColor.RED + "Use" + ChatColor.DARK_RED + " /setwither" + ChatColor.RED + " for give your weapon witherness");
      p.sendMessage(ChatColor.RED + "Use" + ChatColor.DARK_RED + " /setfrozen" + ChatColor.RED + " for give your weapon frozen");
      p.sendMessage(ChatColor.RED + "Use" + ChatColor.DARK_RED + " /setweak" + ChatColor.RED + " for give your weapon weakness");
      p.sendMessage(ChatColor.RED + "Use" + ChatColor.DARK_RED + " /setconfusion" + ChatColor.RED + " for give your weapon confusion");
      p.sendMessage(ChatColor.RED + "Use" + ChatColor.DARK_RED + " /setpoisonbow" + ChatColor.RED + " for give your bow poison shoot");
      p.sendMessage(ChatColor.RED + "Use" + ChatColor.DARK_RED + " /setblindbow" + ChatColor.RED + " for give your bow blind shoot");
      p.sendMessage(ChatColor.RED + "Use" + ChatColor.DARK_RED + " /setwitherbow" + ChatColor.RED + " for give your bow wither shoot");
      p.sendMessage(ChatColor.RED + "Use" + ChatColor.DARK_RED + " /setfrozenbow" + ChatColor.RED + " for give your bow frozen shoot");
      p.sendMessage(ChatColor.RED + "Use" + ChatColor.DARK_RED + " /setweakbow" + ChatColor.RED + " for give your bow weak shoot");
      p.sendMessage(ChatColor.RED + "Use" + ChatColor.DARK_RED + " /setconfusionbow" + ChatColor.RED + " for give your weapon confusion shoot");
      p.sendMessage("§6§m§l---------------------------------------------");
      return true;
    }

    return false;
  }
}