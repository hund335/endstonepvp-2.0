package effects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Wither1
  implements CommandExecutor
{
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player)) {
      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
      return true;
    }
    Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("SetWither")) {
      if (!p.hasPermission("weapon.wither")) {
        p.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
        return true;
      }

      if (args.length == 0) {
        p.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /" + commandLabel + " <level>");
        return true;
      }
      if (args.length == 1) {
        ItemStack is = p.getItemInHand();
        try {
          int i = Integer.parseInt(args[0]);
          doEnchant(p, is, i);
        } catch (NumberFormatException ex) {
          p.sendMessage(ChatColor.RED + args[0] + " unknown number");
        }
      }
    }
    return true;
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
public void doEnchant(Player p, ItemStack is, int i)
  {
    if (i == 1) {
      ItemMeta im = is.getItemMeta();

      List lore = new ArrayList();
      lore.add(ChatColor.GRAY + "Witherness I");
      im.setLore(lore);
      is.setItemMeta(im);
      p.sendMessage("§4Endstone§cPvP §4§l» §7You added Witherness to your item.");
      p.updateInventory();
    }
    else if (i == 2) {
      ItemMeta im = is.getItemMeta();

      List lore = new ArrayList();
      lore.add(ChatColor.GRAY + "Witherness II");
      im.setLore(lore);
      is.setItemMeta(im);
      p.sendMessage("§4Endstone§cPvP §4§l» §7You added Witherness to your item.");
      p.updateInventory();
    }
    else if (i == 3) {
      ItemMeta im = is.getItemMeta();

      List lore = new ArrayList();
      lore.add(ChatColor.GRAY + "Witherness III");
      im.setLore(lore);
      is.setItemMeta(im);
      p.sendMessage("§4Endstone§cPvP §4§l» §7You added Witherness to your item.");
      p.updateInventory();
    } else {
      p.sendMessage(ChatColor.RED + "The levels: 1, 2, 3.");
    }
  }
}