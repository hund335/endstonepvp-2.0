package net.amoebaman.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;

/**
 * A class containing static utility methods and caches which are intended as reflective conveniences.
 * Unless otherwise noted, upon failure methods will return {@code null}.
 */
public final class Reflection {

	private static String _versionString;
	
	private Reflection(){
		
	}
	
	/**
	 * Gets the version string from the package name of the CraftBukkit server implementation.
	 * This is needed to bypass the JAR package name changing on each update.
	 * @return The version string of the OBC and NMS packages, <em>including the trailing dot</em>.
	 */
	public synchronized static String getVersion() {
		if(_versionString == null){
			if(Bukkit.getServer() == null){
				// The server hasn't started, static initializer call?
				return null;
			}
			String name = Bukkit.getServer().getClass().getPackage().getName();
			_versionString = name.substring(name.lastIndexOf('.') + 1) + ".";
		}
		
		return _versionString;
	}

	/**
	 * Stores loaded classes from the {@code net.minecraft.server} package.
	 */
	private static final Map<String, Class<?>> _loadedNMSClasses = new HashMap<String, Class<?>>();
	/**
	 * Stores loaded classes from the {@code org.bukkit.craftbukkit} package (and subpackages).
	 */
	private static final Map<String, Class<?>> _loadedOBCClasses = new HashMap<String, Class<?>>();
	
	/**
	 * Gets a {@link Class} object representing a type contained within the {@code net.minecraft.server} versioned package.
	 * The class instances returned by this method are cached, such that no lookup will be done twice (unless multiple threads are accessing this method simultaneously).
	 * @param className The name of the class, excluding the package, within NMS.
	 * @return The class instance representing the specified NMS class, or {@code null} if it could not be loaded.
	 */
	public synchronized static Class<?> getNMSClass(String className) {
		if(_loadedNMSClasses.containsKey(className)){
			return _loadedNMSClasses.get(className);
		}
		
		String fullName = "net.minecraft.server." + getVersion() + className;
		Class<?> clazz = null;
		try {
			clazz = Class.forName(fullName);
		} catch (Exception e) {
			e.printStackTrace();
			_loadedNMSClasses.put(className, null);
			return null;
		}
		_loadedNMSClasses.put(className, clazz);
		return clazz;
	}

	/**
	 * Gets a {@link Class} object representing a type contained within the {@code org.bukkit.craftbukkit} versioned package.
	 * The class instances returned by this method are cached, such that no lookup will be done twice (unless multiple threads are accessing this method simultaneously).
	 * @param className The name of the class, excluding the package, within OBC. This name may contain a subpackage name, such as {@code inventory.CraftItemStack}.
	 * @return The class instance representing the specified OBC class, or {@code null} if it could not be loaded.
	 */
	public synchronized static Class<?> getOBCClass(String className) {
		if(_loadedOBCClasses.containsKey(className)){
			return _loadedOBCClasses.get(className);
		}
		
		String fullName = "org.bukkit.craftbukkit." + getVersion() + className;
		Class<?> clazz = null;
		try {
			clazz = Class.forName(fullName);
		} catch (Exception e) {
			e.printStackTrace();
			_loadedOBCClasses.put(className, null);
			return null;
		}
		_loadedOBCClasses.put(className, clazz);
		return clazz;
	}

	/**
	 * Attempts to get the NMS handle of a CraftBukkit object.
	 * <p>
	 * The only match currently attempted by this method is a retrieval by using a parameterless {@code getHandle()} method implemented by the runtime type of the specified object.
	 * </p>
	 * @param obj The object for which to retrieve an NMS handle.
	 * @return The NMS handle of the specified object, or {@code null} if it could not be retrieved using {@code getHandle()}.
	 */
	public synchronized static Object getHandle(Object obj) {
		try {
			return getMethod(obj.getCl