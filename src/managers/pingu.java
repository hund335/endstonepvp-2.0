package managers;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import cmds.ping;

@SuppressWarnings("unused")
public class pingu implements Listener
{
  
  private static Class<?> craftPlayer;
  private static Method handle;
  private static Field pingField;
  
  static
  {
    try
    {
      String serverVersion = getServerVersion();
      craftPlayer = Class.forName("org.bukkit.craftbukkit." + serverVersion + 
        ".entity.CraftPlayer");
      Class<?> entityPlayer = Class.forName("net.minecraft.server." + serverVersion + 
        ".EntityPlayer");
      
      handle = craftPlayer.getMethod("getHandle", new Class[0]);
      
      pingField = entityPlayer.getField("ping");
    }
    catch (Exception e)
    {
     
      e.printStackTrace();
    }
  }
  
  public static void msg(CommandSender target, String message)
  {
    //target.sendMessage(ChatColor.translateAlternateColorCodes('&', message, 
      
  }
  
 
  
  public static int getPlayerPing(Player player)
    throws Exception
  {
    int ping = 0;
    
    Object converted = craftPlayer.cast(player);
    
    Object entityPlayer = handle.invoke(converted, new Object[0]);
    
    ping = pingField.getInt(entityPlayer);
    
    return ping;
  }
  
  public static String getServerVersion()
  {
    Pattern brand = Pattern.compile("(v|)[0-9][_.][0-9][_.][R0-9]*");
    
    String pkg = Bukkit.getServer().getClass().getPackage().getName();
    String version = pkg.substring(pkg.lastIndexOf('.') + 1);
    if (!brand.matcher(version).matches()) {
      version = "";
    }
    return version;
  }
}
