package cmds;
 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
 
public class youtube
implements CommandExecutor
    {
      String prefix = ChatColor.GRAY + "[" + ChatColor.DARK_GRAY + "EndstonePvP" + ChatColor.GRAY + "]";
      
      
      public final Logger logger = Logger.getLogger("Minecraft");
 
      @SuppressWarnings({ "rawtypes", "unchecked" })
    public Map<String, String> oto = new HashMap();
      public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
      
      public void onDisable()
      {
        

      }
 
      public void onEnable()
      {
        
      }
       
        public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
                return true;
        }
           final Player p = (Player)sender;
            if (commandLabel.equalsIgnoreCase("youtube")){
                if(p.hasPermission("youtube.use")){
                    if (args.length == 0) {
                    	int cooldownTime1 = 6; // Get number of seconds from wherever you want
                        if(cooldowns.containsKey(sender.getName())) {
                            long secondsLeft = ((cooldowns.get(sender.getName())/3600000)+cooldownTime1) - (System.currentTimeMillis()/3600000);
                            if(secondsLeft>0) {
                                // Still cooling down
                                p.sendMessage("§4Endstone§cPvP §4§l» §cYou need to wait §4§o" + secondsLeft + " hours§c.");
                                return true;
                            }
                        }
                        // No cooldown found or cooldown has expired, save new cooldown
                        cooldowns.put(sender.getName(), System.currentTimeMillis());
    
                             
                           PlayerInventory pi = p.getInventory();
                           p.sendMessage("§4Endstone§cPvP §4§l» §7You have successfully claimed your Youtube kit.");
                           ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);
                           sword.addEnchantment(Enchantment.DAMAGE_ALL, 4);
                           ItemMeta swordmeta = sword.getItemMeta();
                           swordmeta.setDisplayName(ChatColor.YELLOW + "Sword");
                           List<String> lore = new ArrayList<String>();
                           swordmeta.setLore(lore);
                           sword.setItemMeta(swordmeta);
                           pi.addItem(sword);
                           //Helmet
                           ItemStack helm = new ItemStack(Material.DIAMOND_HELMET, 1);
                           helm.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
                           ItemMeta helmmeta = helm.getItemMeta();
                           helmmeta.setDisplayName(ChatColor.YELLOW + "Helmet");
                           List<String> lore1 = new ArrayList<String>();
                           helmmeta.setLore(lore1);
                           helm.setItemMeta(helmmeta);
                           pi.addItem(helm);                   
                           //Chestplate
                           ItemStack cp = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
                           cp.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
                           ItemMeta cpmeta = cp.getItemMeta();
                           cpmeta.setDisplayName(ChatColor.YELLOW + 