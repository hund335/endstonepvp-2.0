package cmds;
 
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import javax.activation.CommandObject;

import mkremins.fanciful.FancyMessage;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

 
@SuppressWarnings("unused")
public class rules
implements CommandExecutor
{
  
 
private static final String chatObject = null;

  {

  }

 
@EventHandler(priority = EventPriority.HIGH)

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		  {
			  if (!(sender instanceof Player)) {
			      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
			      return true;
			    }
		
	    Player p = (Player)sender;
	
	    if (commandLabel.equalsIgnoreCase("rules")) {
	      if (p.hasPermission("rules.use")) {

	        if (args.length == 0) {
	
	        	Player p1 = (Player)sender;
	                        
	       
	        	
	p1.sendMessage("§6§m§l---------------------------------------------");
	p1.sendMessage("§4Rules");
	p1.sendMessage("");
	p1.sendMessage("§cPoint your mouse on a rule to view detailed infomation.");
	new FancyMessage("")
    .then("§c§l1. §4§lClients mods are not allowed.")
    .color(ChatColor.BLUE)
    .tooltip("§4Rule 1\n§7Most client mods are not allowed, however\n§7the mods in this list are allowed:\n§4- OptiFine\n§7\n§7'hacking' or 'cheating' client mods\n§7strictly forbidden. 'Macro' programs\n§7For keyboards and mic are also\n§7considered as cheating.")
    .send(sender);
new FancyMessage("")
    .then("§c§l2. §4§lKeep the chat clean.")
    .color(ChatColor.BLUE)
    .tooltip("§4Rule 2\n§7Speak mature in chat. Talk English/Danish, so all players\n§7can understand you.\n§7Do not use swearing words, caps and dont spam.\n§7You will get warned or muted when you break this rule.\n§7Do not complain. After all, this is just a game.")
    .send(sender);
new FancyMessage("")
    .then("§c§l3. §4§lShow your respect..")
    .color(ChatColor.BLUE)
    .tooltip("§4Rule 3\n§7Respect players and staff\n§7This includes scamming players.\n§7Breaking this rule will result in a mute.")
    .send(sender);
new FancyMessage("")
    .then("§c§l4. §4§lDont be an abuser.")
    .color(ChatColor.BLUE)
    .tooltip("§4Rule 4\n§7Abusing bugs and glitches in the server is bannable.\n§7Instead, report bugs to the Staff to receive a reward!")
    .send(sender);
new FancyMessage("")
    .then("§c§l5. §4§lPlay fair in PvP.")
    .color(ChatColor.BLUE)
    .tooltip("§4Rule 5\n§7As said above, do not use hacking/cheating mods and do not\n§7abuse bugs that give you unfair PvP advantages.\n§7We're striving for a fair PvP experience against\n§7all players, and we ban players that ruin this.")
    .send(sender);
    p1.sendMessage("");
    p1.sendMessage("§cThanks for reading the rules.");
    p1.sendMessage("§cRemember that this is a game and we're all hereto have fun.");
    p1.sendMessage("§6§m§l---------------------------------------------");

	}

}
	    }
		  }
		return false;
}
}