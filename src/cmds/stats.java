package cmds;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import managers.pingu;
import mc.hund35.main.main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class stats
implements CommandExecutor
{

  @SuppressWarnings({ "unchecked", "rawtypes" })
public Map<String, String> oto = new HashMap();

 

  @Override
  public boolean onCommand(CommandSender sender, Command command,
          String label, String[] args) {

	  
    Player p = (Player)sender;
   

    if (!(sender instanceof Player))
    {
      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
      return true;
    }
    if (sender.getName().equalsIgnoreCase("Stats"))
    {
    	
    	
    }
    if (args.length == 0) {
    	
    	Player player = (Player)sender;
	    File file = new File(main.load.getDataFolder().getPath(), "stats.yml");
		  YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    	
    	sender.sendMessage("§6§m§l---------------------------------------------");
	    sender.sendMessage(ChatColor.RED + "Stats of " + 
	      ChatColor.DARK_RED + player.getName());
	    sender.sendMessage("");
	    sender.sendMessage(ChatColor.RED + "Kills: " + ChatColor.DARK_RED + 
	      config.getInt("Stats." + player.getName() + ".Kills"));
	    sender.sendMessage(ChatColor.RED + "Deaths: " + ChatColor.DARK_RED + 
	  	      config.getInt("Stats." + player.getName() + ".Deaths"));
	    sender.sendMessage("§6§m§l---------------------------------------------");
    	
        return false;
    }
    if(args.length >= 1) {
        String targetName = args[0];
        Player player = (Player) sender;
        Player target = Bukkit.getPlayer(targetName);
        if (target == null) {
            player.sendMessage("§4Endstone§cPvP §4§l» §4" + targetName + " §cis offline!");
            return true;
        }
    
    	 
	    File file = new File(main.load.getDataFolder().getPath(), "stats.yml");
		  YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    
    
    sender.sendMessage("§6§m§l---------------------------------------------");
    sender.sendMessage(ChatColor.RED + "Stats of " + 
      ChatColor.DARK_RED + targetName);
    sender.sendMessage("");
    sender.sendMessage(ChatColor.RED + "Kills: " + ChatColor.DARK_RED + 
  	      config.getInt("Stats." + targetName + ".Kills"));
    sender.sendMessage(ChatColor.RED + "Deaths: " + ChatColor.DARK_RED + 
  	      config.getInt("Stats." + targetName + ".Deaths"));
    sender.sendMessage("§6§m§l---------------------------------------------");

    
  }
	
	  
    
	return false;

  }
}
