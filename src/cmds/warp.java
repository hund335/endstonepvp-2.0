package cmds;

import java.util.UUID;

import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import configs.warps;

@SuppressWarnings("unused")
public class warp
  implements CommandExecutor
{
  warps settings = warps.getInstance();
  
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
    	sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
      return true;
    }
    Player p = (Player)sender;
    if (cmd.getName().equalsIgnoreCase("warp"))
    {
      if (args.length == 0)
      {
        p.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /warp <warpname>");
        return true;
      }
      if (this.settings.getData().getConfigurationSection("warps." + args[0]) == null)
      {
        p.sendMessage("§4Endstone§cPvP §4§l» §cWarp: §4" + args[0] + " §cdoes not exist!");
        return true;
      }
      World w = Bukkit.getServer().getWorld(this.settings.getData().getString("warps." + args[0] + ".world"));
      double x = this.settings.getData().getDouble("warps." + args[0] + ".x");
      double y = this.settings.getData().getDouble("warps." + args[0] + ".y");
      double z = this.settings.getData().getDouble("warps." + args[0] + ".z");
      p.teleport(new Location(w, x, y, z));
      p.sendMessage("§4Endstone§cPvP §4§l» §7Warping to: §c" + args[0] + "§7!");
    }
	return false;
  }
}