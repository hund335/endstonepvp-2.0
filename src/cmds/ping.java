package cmds;

import java.util.HashMap;
import java.util.Map;

import managers.pingu;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class ping
implements CommandExecutor
{

  @SuppressWarnings({ "unchecked", "rawtypes" })
public Map<String, String> oto = new HashMap();

 

  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if ((!(sender instanceof Player)) && (args.length == 0))
    {
    	sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
    }
    else if (!sender.hasPermission("ping.use"))
    {
      sender.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
    }
    else if (args.length == 0)
    {
      try
      {
    	  sender.sendMessage("§4Endstone§cPvP §4§l» §7Your ping is: §c" + pingu.getPlayerPing((Player)sender) + "§7ms.");
         
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
    }
    else
    {
      Player player = Bukkit.getPlayer(args[0]);
      if (player != null) {
        try
        {
          sender.sendMessage("§4Endstone§cPvP §4§l» §c" + player.getName() + "'s §7ping is: §c" + pingu.getPlayerPing(player) + "§7ms");
             

        }
        catch (Exception e)
        {
          e.printStackTrace();
        }
      } else {
        sender.sendMessage("§4Endstone§cPvP §4§l» §cPlayer §4" + args[0] + " §cis not online.");
         
      }
    }
    return true;
  }
}
