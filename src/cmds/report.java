package cmds;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class report
implements CommandExecutor
    {

	
		
	
	
	
	@Override
    public boolean onCommand(CommandSender sender, Command command,
            String label, String[] args) {
		
		if (!(sender instanceof Player)) {
		      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
		      return true;
		    }
		
		
        if (!sender.hasPermission("report.use")) {
            sender.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
            return true;
        }
        if (args.length == 0) {
        	sender.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /report <player> <reason>");
            return false;
        }
        if(args.length == 1) {
            sender.sendMessage("§4Endstone§cPvP §4§l» §cYou need a reason for report that player.");
            return true;
        }
        if(args.length >= 2) {
            String targetName = args[0];
            Player player = (Player) sender;
            Player target = Bukkit.getPlayer(targetName);
            if (target == null) {
                player.sendMessage("§4Endstone§cPvP §4§l» §4" + targetName + " §cis offline!");
                return true;
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                sb.append(args[i]);
                if (i < args.length) {
                    sb.append(" ");
                }
            }
            player.sendMessage("§4Endstone§cPvP §4§l» §4You §7have reported §4" + targetName + " §7for §c" + sb.toString());
           
            for(Player player1 : Bukkit.getOnlinePlayers())
            if(player1.hasPermission("report.read")) {
            	player1.sendMessage("§4Endstone§cPvP §4§l» §4" + player.getName() + " §7has reported §4" + targetName + " §7for §c" + sb.toString());
                       
            }
        
        return true;
    }
		return false;

    }
}