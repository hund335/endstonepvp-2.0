package cmds;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class color
implements CommandExecutor
{

  @SuppressWarnings({ "unchecked", "rawtypes" })
public Map<String, String> oto = new HashMap();

 

  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
	  if (!(sender instanceof Player)) {
	      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
	      return true;
	    }
	  
	  
	  Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("color")) {
      if (p.hasPermission("color.use")) {
        if (args.length == 0) {
          p.sendMessage(ChatColor.RED + "/color <color>");
          p.sendMessage(ChatColor.DARK_AQUA + "darkaqua" + ChatColor.DARK_PURPLE + " purple" + ChatColor.WHITE + " white" + ChatColor.GREEN + " lime" + ChatColor.YELLOW + " yellow" + ChatColor.DARK_GREEN + " darkgreen" + ChatColor.DARK_BLUE + " darkblue" + ChatColor.RED + " red" + ChatColor.BLUE + " blue");
          p.sendMessage(ChatColor.GRAY + "off" + ChatColor.LIGHT_PURPLE + " pink" + ChatColor.GOLD + " gold" + ChatColor.AQUA + " aqua");
        }

      }
      else
      {
    	  sender.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
      }

    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("darkaqua")) && 
      (p.hasPermission("color.use")) && 
      (!this.oto.containsKey(p.getName())))
    {
      Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "pex user " + p.getName() + " prefix &3");
      p.sendMessage("§4Endstone§cPvP §4§l» §7Your nickname color was changed to" + ChatColor.DARK_AQUA + " darkaqua");
    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("purple")) && 
      (p.hasPermission("color.use")) && 
      (!this.oto.containsKey(p.getName())))
    {
    	Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "pex user " + p.getName() + " prefix &5");
      p.sendMessage("§4Endstone§cPvP §4§l» §7Your nickname color was changed to" + ChatColor.DARK_PURPLE + " purple");
    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("white")) && 
      (p.hasPermission("color.use")) && 
      (!this.oto.containsKey(p.getName())))
    {
    	Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "pex user " + p.getName() + " prefix &f");
      p.sendMessage("§4Endstone§cPvP §4§l» §7Your nickname color was changed to" + ChatColor.WHITE + " white");
    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("lime")) && 
      (p.hasPermission("color.use")) && 
      (!this.oto.containsKey(p.getName())))
    {
    	Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "pex user " + p.getName() + " prefix &a");
      p.sendMessage("§4Endstone§cPvP §4§l» §7Your nickname color was changed to" + ChatColor.GREEN + " lime");
    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("yellow")) && 
      (p.hasPermission("color.use")) && 
      (!this.oto.containsKey(p.getName())))
    {
    	Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "pex user " + p.getName() + " prefix &e");
      p.sendMessage("§4Endstone§cPvP §4§l» §7Your nickname color was changed to" + ChatColor.YELLOW + " yellow");
    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("darkgreen")) && 
      (p.hasPermission("color.use")) && 
      (!this.oto.containsKey(p.getName())))
    {
    	Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "pex user " + p.getName() + " prefix &2");
      p.sendMessage("§4Endstone§cPvP §4§l» §7Your nickname color was changed to" + ChatColor.DARK_GREEN + " darkgreen");
    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("darkblue")) &&