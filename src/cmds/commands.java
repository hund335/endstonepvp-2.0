package cmds;

import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commands
implements CommandExecutor
{

  @SuppressWarnings({ "unchecked", "rawtypes" })
public Map<String, String> oto = new HashMap();

 

  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
	  if (!(sender instanceof Player)) {
	      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
	      return true;
	    }
	  
	  
	  Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("commands")) {
      if (p.hasPermission("commands.use")) {
        if (args.length == 0) {
        	p.sendMessage("§6§m§l---------------------------------------------");
          p.sendMessage("§4/commands player §c- §7for show a list of player cmds.");
          p.sendMessage("§4/commands donator §c- §7for show a list of donator cmds.");
          p.sendMessage("§4/commands builder §c- §7for show a list of builder cmds.");
          p.sendMessage("§4/commands staff §c- §7for show a list of staff cmds.");
          p.sendMessage("§6§m§l---------------------------------------------");
        }

      }
      else
      {
    	  sender.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
      }

    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("player")) && 
      (p.hasPermission("commands.use")) && 
      (!this.oto.containsKey(p.getName())))
    {
    	p.sendMessage("§6§m§l---------------------------------------------");
    	p.sendMessage("§4/rules §c- §7for read the server rules.");
    	p.sendMessage("§4/report <player> <message> §c- §7for report a rule breaker.");
    	p.sendMessage("§4/msg <player> <message> §c- §7for send a private message.");
    	p.sendMessage("§4/spawn §c- §7for goto spawn.");
    	p.sendMessage("§4/shop §c- §7for open the shop.");
    	p.sendMessage("§4/vote §c- §7for get cool rewards.");
    	p.sendMessage("§4/buy §c- §7for upgrade your account.");
    	p.sendMessage("§4/trade <player> §c- §7for send a trade request.");
    	p.sendMessage("§6§m§l---------------------------------------------");
    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("donator")) && 
      (p.hasPermission("commands.use")) && 
      (!this.oto.containsKey(p.getName())))
    {
    	p.sendMessage("§6§m§l---------------------------------------------");
    	p.sendMessage("§4/color <color> §c- §7change your name color.");
    	p.sendMessage("§4/claimkey <rank> §c- §7for claim your daily key.");
    	p.sendMessage("§4/healme §c- §7for heal your health (Emperor).");
    	p.sendMessage("§4/potions <player> §c- §7for check what effects the target player has (Emperor).");
    	p.sendMessage("§6§m§l---------------------------------------------"); 
    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("builder")) && 
      (p.hasPermission("commands.use")) && 
      (!this.oto.containsKey(p.getName())))
    {
    	p.sendMessage("§6§m§l---------------------------------------------");
    	p.sendMessage("§cComing soon");
    	p.sendMessage("§6§m§l---------------------------------------------");

    }
    if ((args.length == 1) && 
    	      (args[0].equalsIgnoreCase("staff")) && 
    	      (p.hasPermission("commands.use")) && 
    	      (!this.oto.containsKey(p.getName())))
    	    {
    	    	p.sendMessage("§6§m§l---------------------------------------------");
    	    	p.sendMessage("§4/hack <player> §c- §7for punish hackers (Helper).");
    	    	p.sendMessage("§4/watch <player> §c- §7for watch a player (Helper).");
    	    	p.sendMessage("§4/spam <player> §c- §7for punish spammers (Helper).");
    	    	p.sendMessage("§4/warn <player> <reason> §c- §7for warn a player (Helper).");
    	    	p.sendMessage("§4/kick <player> <reason> §c- §7for kick a player (M