package cmds;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

@SuppressWarnings("unused")
public class claimkey
implements CommandExecutor
{

  @SuppressWarnings({ "unchecked", "rawtypes" })
public Map<String, String> oto = new HashMap();
  public HashMap<String, Long> cooldowns1 = new HashMap<String, Long>();
  public HashMap<String, Long> cooldowns2 = new HashMap<String, Long>();
  public HashMap<String, Long> cooldowns3 = new HashMap<String, Long>();

  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
	  if (!(sender instanceof Player)) {
	      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
	      return true;
	    }
	  
	  
	  Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("claimkey")) {
      if (p.hasPermission("claimkey.use")) {
    	  if (args.length < 1) {
              sender.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /claimkey <rank>");
              return true;
        }

      }
      else
      {
    	  sender.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
      }

    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("master")) && 
      (p.hasPermission("claimkey.master")) && 
      (!this.oto.containsKey(p.getName()))){
    	int cooldownTime1 = 24; // Get number of seconds from wherever you want
    	if(cooldowns1.containsKey(sender.getName())) {
    	 long secondsLeft = ((cooldowns1.get(sender.getName())/3600000)+cooldownTime1) - (System.currentTimeMillis()/3600000);
    	 if(secondsLeft>0) {
    	     // Still cooling down
    		 p.sendMessage("§4Endstone§cPvP §4§l» §cYou need to wait §4§o" + secondsLeft + " hours§c.");
    	     return true;
                 }
             }
             // No cooldown found or cooldown has expired, save new cooldown
             cooldowns1.put(sender.getName(), System.currentTimeMillis());
             p.sendMessage("§4Endstone§cPvP §4§l» §7You have successfully claimed your daily SilverKey.");
    	PlayerInventory pi = p.getInventory(); 
	        ItemStack card = new ItemStack(Material.NAME_TAG,1);
	         ItemMeta cardmeta = card.getItemMeta();
	         cardmeta.setDisplayName("§8Silver§7Key");
	         card.setItemMeta(cardmeta);
	         pi.addItem(card);
    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("hero")) && 
      (p.hasPermission("claimkey.hero")) && 
      (!this.oto.containsKey(p.getName()))){
    	int cooldownTime1 = 24; // Get number of seconds from wherever you want
    	if(cooldowns2.containsKey(sender.getName())) {
    	 long secondsLeft = ((cooldowns2.get(sender.getName())/3600000)+cooldownTime1) - (System.currentTimeMillis()/3600000);
    	 if(secondsLeft>0) {
    	     // Still cooling down
    		 p.sendMessage("§4Endstone§cPvP §4§l» §cYou need to wait §4§o" + secondsLeft + " hours§c.");
    	     return true;
                 }
             }
             // No cooldown found or cooldown has expired, save new cooldown
             cooldowns2.put(sender.getName(), System.currentTimeMillis());
             p.sendMessage("§4Endstone§cPvP §4§l» §7You have successfully claimed your daily GoldKey.");
    	PlayerInventory pi = p.getInventory(); 
	        ItemStack card = new ItemStack(Material.NAME_TAG,1);
	         ItemMeta cardmeta = card.getItemMeta();
	         cardmeta.setDisplayName("§6Gold§eKey");
	         card.setItemMeta(cardmeta);
	         pi.addItem(card);
    }
    if ((args.length == 1) && 
    	      (args[0].equalsIgnoreCase("legend")) && 
    	      (p.hasPermission("claimkey.legend")) && 
    	      (!this.oto.containsKey(p.getName()))){
    	int cooldown