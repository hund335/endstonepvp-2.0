package cmds;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class list
implements CommandExecutor
{
	private final String line = new StringBuilder().append(ChatColor.GOLD).append("§6§m§l---------------------------------------------").toString();
	  private final String all = new StringBuilder().append(ChatColor.RED).append("Connected players: ").append(ChatColor.DARK_RED).toString();
	  private final String donators = new StringBuilder().append(ChatColor.RED).append("Connected donator players: ").append(ChatColor.DARK_RED).toString();
	  private final String staff = new StringBuilder().append(ChatColor.RED).append("Connected staff players: ").append(ChatColor.DARK_RED).toString();
	  @SuppressWarnings("unused")
	private final String separator = ", ";

	  @SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	  {
	    if (label.equalsIgnoreCase("list"))
	    {
	      List aList = new ArrayList();
	      List dList = new ArrayList();
	      List sList = new ArrayList();

	      for (Player p : Bukkit.getOnlinePlayers()) {
	        aList.add(p.getName());
	        if (p.hasPermission("list.staff")) {
	          sList.add(p.getName());
	        }
	        else if (p.hasPermission("list.donator")) {
	          dList.add(p.getName());
	        }
	      }

	      sender.sendMessage(this.line);
	      sender.sendMessage(countSentence(aList.size(), "player", "players"));
	      sender.sendMessage(countSentence(sList.size(), "staff member", "staff members"));
	      sender.sendMessage(countSentence(dList.size(), "donator", "donators"));

	      StringBuilder sb = new StringBuilder();

	      if (args.length > 0)
	      {
	        if (args[0].equalsIgnoreCase("donator")) {
	          sb.append(this.donators).append(implode(dList));
	        }
	        else if (args[0].equalsIgnoreCase("staff")) {
	          sb.append(this.staff).append(implode(sList));
	        }
	        else {
	          sb.append(this.all).append(implode(aList));
	        }
	      }
	      else {
	        sb.append(this.all).append(implode(aList));
	      }

	      sender.sendMessage(sb.toString());

	      sender.sendMessage(this.line);
	      return true;
	    }
	    return false;
	  }

	  private String countSentence(int number, String type, String typePlural) {
	    return new StringBuilder().append(ChatColor.RED).append("There ").append(number == 1 ? "is " : "are ").append(ChatColor.DARK_RED).append(number).append(ChatColor.RED).append(" ").append(number == 1 ? type : typePlural).append(" online.").toString();
	  }

	  private String implode(Collection<String> strs)
	  {
	    StringBuilder result = new StringBuilder();
	    for (String s : strs) {
	      result.append(s).append(", ");
	    }
	    if (result.length() < 2) {
	      return "";
	    }
	    return result.substring(0, result.length() - ", ".length());
	  }
	}