package admincmds;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import configs.warps;

@SuppressWarnings("unused")
public class delwarp
  implements CommandExecutor
{
	 warps settings = warps.getInstance();
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
    	sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
      return true;
    }
    Player p = (Player)sender;
    if (cmd.getName().equalsIgnoreCase("delwarp"))
    {
      if (args.length == 0)
      {
    	  p.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /delwarp <warpname>");
   
        return true;
      }
      if (this.settings.getData().getConfigurationSection("warps." + args[0]) == null)
      {
        p.sendMessage("§4Endstone§cPvP §4§l» §cWarp: §4" + args[0] + " §cdoes not exist!");
        return true;
      }
      this.settings.getData().set("warps." + args[0], null);
      this.settings.saveData();
      p.sendMessage("§4Endstone§cPvP §4§l» §7Removed warp: §c" + args[0] + "§7!");
    }
    return true;
  }
}
