package admincmds;
 
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
 
public class tphere
implements CommandExecutor, Listener
    {
       
        public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        	  if (!(sender instanceof Player)) {
                  sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
                  return true;
          }
        	  
                if (cmd.getName().equalsIgnoreCase("tphere")) {
                	 if(sender.hasPermission("tphere.use")){
                      
                	if (args.length < 1) {
                		
                                sender.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /tphere <player>");
                                return true;
                        }      
                	}
                }
                
  	  	      if(args.length >= 1) {
  	          String targetName = args[0];
  	          Player player = (Player) sender;
  	          Player target = Bukkit.getPlayer(targetName);
  	          if (target == null) {
  	              player.sendMessage("§4Endstone§cPvP §4§l» §4" + targetName + " §cis offline!");
  	              
  	              return true;
  	          }
  	        if (target == player)
            {
              player.sendMessage("§4Endstone§cPvP §4§l» §cYou cannot teleport you to yourself!");
              
              return false;
            }
  	        
  	  	  Bukkit.broadcast("§4Endstone§cPvP §4§l» §c" + targetName + " §7has been teleported to: §c" + player.getName() + "§7!", "tphere.see");
  	  	      player.sendMessage("§4Endstone§cPvP §4§l» §7Teleporting §c" + targetName + " §7to §cyou§7.");
  	      target.teleport(player);
                        
                       
  	  	      
                
  	  	  } else {
             sender.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
              
          }
			return false;
        }
    }
        
