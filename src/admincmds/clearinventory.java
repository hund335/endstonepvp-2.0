package admincmds;
 
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
 
public class clearinventory
implements CommandExecutor, Listener
    {
       
        public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        	  if (!(sender instanceof Player)) {
                  sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
                  return true;
          }
        	  
                if (cmd.getName().equalsIgnoreCase("clear")) {
                	 if(sender.hasPermission("clear.use")){
                      
                	if (args.length < 2) {
                		
                                sender.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /clear <player> <reason>");
                                return true;
                        }      
                	}
                }
                
  	  	      if(args.length >= 1) {
  	          String targetName = args[0];
  	          Player player = (Player) sender;
  	          Player target = Bukkit.getPlayer(targetName);
  	          if (target == null) {
  	              player.sendMessage("§4Endstone§cPvP §4§l» §4" + targetName + " §cis offline!");
  	              
  	              return true;
  	          }
  	  	  
   	         if(target.hasPermission("clear.no")){
   	        	 sender.sendMessage("§4Endstone§cPvP §4§l» §cYou cannot clear that player's inventory!");
   	        	 return true;
   	         }
  	        if(args.length >= 2) {
                        String msg = "";
                        for (int i = 1; i < args.length; i++) {
                                msg += args[i] + " ";
                                
                        }
                        Bukkit.broadcast("§4Endstone§cPvP §4§l» §c" + sender.getName() + " §7cleared §c" + targetName + "'s §7inventory for: §c" + msg, "clear.see");
                        sender.sendMessage("§4Endstone§cPvP §4§l» §7You have cleared §c" + targetName + "'s §7inventory for: §c" + msg);
                        target.sendMessage("§4Endstone§cPvP §4§l» §cYour §7inventory has been cleared for: §c" + msg);
                        PlayerInventory pi = target.getInventory(); 
                        pi.clear();
                        target.getInventory().setHelmet(new ItemStack (Material.AIR));
                        target.getInventory().setChestplate(new ItemStack (Material.AIR));
                        target.getInventory().setLeggings(new ItemStack (Material.AIR));
                        target.getInventory().setBoots(new ItemStack (Material.AIR));
                       
                }
  	  	  } else {
             sender.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
              
          }
  	  	      
				return false;
        }
}