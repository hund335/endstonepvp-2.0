package admincmds;

import mc.hund35.main.main;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class enderchest
  implements CommandExecutor, Listener
{
  private final main plugin;

  public enderchest(main plugin)
  {
    this.plugin = plugin;
  }

  public void onInventoryClose(InventoryCloseEvent event)
  {
    Player player = (Player)event.getPlayer();
    if (this.plugin.invFile.isEditing(player.getName()))
    {
      this.plugin.invFile.saveInventory(this.plugin.invFile.getEditTarget(player.getName()), event.getView().getTopInventory());
      this.plugin.invFile.stopEditing(player.getName());
      return;
    }
    if (!this.plugin.invFile.hasInventoryOpened(player.getName())) {
      return;
    }
    this.plugin.invFile.saveInventory(player.getName(), event.getView().getTopInventory());
    this.plugin.invFile.setInventoryOpened(player.getName(), Boolean.valueOf(false));
    player.playSound(player.getLocation(), Sound.BLOCK_CHEST_CLOSE, 1.0F, 1.0F);
  }
  
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!onCommandExecute(sender, cmd, commandLabel, args)) {
      sender.sendMessage(this.plugin.color(this.plugin.getCommand(commandLabel).getUsage()));
    }
    return true;
  }

  public boolean onCommandExecute(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!commandLabel.equalsIgnoreCase("chest")) {
      return true;
    }
    if (!(sender instanceof Player)) {
      return true;
    }
    Player player = (Player)sender;
    if (args.length == 1)
    {
      String target = args[0];
      if (target.equals(player.getName())) {
        return false;
      }
      if (this.plugin.invFile.hasInventoryOpened(target))
      {
        player.sendMessage(this.plugin.color("§cThe target player is currently viewing his enderchest."));
        return true;
      }
      this.plugin.invFile.editInventory(player, target);
    }
    else
    {
      if (this.plugin.invFile.isBeingEdited(player.getName()))
      {
        player.sendMessage("§cYour enderchest is currently being edited by");
        player.sendMessage("§can administrator, try opening it again later.");
        return true;
      }
      this.plugin.invFile.setInventoryOpened(player.getName(), Boolean.valueOf(true));
      player.openInventory(this.plugin.invFile.getInventory(player.getName()));
    }
    return true;
  }
}