package admincmds;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class givekey
implements CommandExecutor
{
 

  @SuppressWarnings({ "unchecked", "rawtypes" })
public Map<String, String> oto = new HashMap();
  


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
	   
		if (!(sender instanceof Player)) {
		      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
		      return true;
		    }
		
		Player p = (Player)sender;
	    if (commandLabel.equalsIgnoreCase("givekey")) {
	      if (p.hasPermission("givekey.use")) {
	    	  if (args.length < 2) {
                  sender.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /givekey <player> <tier>");
                  return true;

	  	      }
	  	      if(args.length >= 1) {
	          String targetName = args[0];
	          Player player = (Player) sender;
	          Player target = Bukkit.getPlayer(targetName);
	          if (target == null) {
	              player.sendMessage("§4Endstone§cPvP §4§l» §4" + targetName + " §cis offline!");
	              
	              return true;
	          }
	          
	      

	      if ((args.length >= 2) && 
	        (args[1].equalsIgnoreCase("silver")) && 
	        (p.hasPermission("givekey.use")) && 
	        (!this.oto.containsKey(sender.getName())))
	      {
	    	  sender.sendMessage("§4Endstone§cPvP §4§l» §7You gave §c" + targetName +" §7a §cSilverKey§7.");
	    	  
		        target.sendMessage("§4Endstone§cPvP §4§l» §7You received a §cSilverKey§7.");
		        PlayerInventory pi = target.getInventory(); 
	  	        ItemStack card = new ItemStack(Material.NAME_TAG,1);
	  	         ItemMeta cardmeta = card.getItemMeta();
	  	         cardmeta.setDisplayName("§8Silver§7Key");
	  	         card.setItemMeta(cardmeta);
	  	         pi.addItem(card);
	      }
	      if ((args.length >= 2) && 
	  	        (args[1].equalsIgnoreCase("gold")) && 
	  	        (p.hasPermission("givekey.use")) && 
	  	        (!this.oto.containsKey(sender.getName())))
	  	      {
	  	    	  sender.sendMessage("§4Endstone§cPvP §4§l» §7You gave §c" + targetName +" §7a §cGoldKey§7.");
	  	    	  
	  		        target.sendMessage("§4Endstone§cPvP §4§l» §7You received a §cGoldKey§7.");
	  		        PlayerInventory pi = target.getInventory(); 
	  	  	        ItemStack card = new ItemStack(Material.NAME_TAG,1);
	  	  	         ItemMeta cardmeta = card.getItemMeta();
	  	  	         cardmeta.setDisplayName("§6Gold§eKey");
	  	  	         card.setItemMeta(cardmeta);
	  	  	         pi.addItem(card);
	  	      }
	      if ((args.length >= 2) && 
		  	        (args[1].equalsIgnoreCase("diamond")) && 
		  	        (p.hasPermission("givekey.use")) && 
		  	        (!this.oto.containsKey(sender.getName())))
		  	      {
		  	    	  sender.sendMessage("§4Endstone§cPvP §4§l» §7You gave §c" + targetName +" §7a §cDiamondKey§7.");
		  	    	  
		  		        target.sendMessage("§4Endstone§cPvP §4§l» §7You received a §cDiamondKey§7.");
		  		        PlayerInventory pi = target.getInventory(); 
		  	  	        ItemStack card = new ItemStack(Material.NAME_TAG,1);
		  	  	         ItemMeta cardmeta = card.getItemMeta();
		  	  	         cardmeta.setDisplayName("§3Diamond§bKey");
		  	  	         card.setItemMeta(cardmeta);
		  	  	         pi.addItem(card);
		  	      }
                    
            	 
	      }


	      }
		
  


}
		return false;
		
		 		
}
}