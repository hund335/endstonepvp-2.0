package admincmds;
 
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
 
public class ip
implements CommandExecutor, Listener
    {
       
        public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        	  if (!(sender instanceof Player)) {
                  sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
                  return true;
          }
        	  
                if (cmd.getName().equalsIgnoreCase("ip")) {
                	 if(sender.hasPermission("ip.use")){
                		  Player player = (Player) sender;
                	if (args.length < 1) {
                		
                                sender.sendMessage("§4Endstone§cPvP §4§l» §7Your IP: §c" + player.getAddress());
                                return true;
                        }      
                	}
                }
                
  	  	      if(args.length >= 1) {
  	          String targetName = args[0];
  	        Player player = (Player) sender;
  	          Player target = Bukkit.getPlayer(targetName);
  	          if (target == null) {
  	              player.sendMessage("§4Endstone§cPvP §4§l» §4" + targetName + " §cis offline!");
  	              
  	              return true;
  	          }
  	  	  
  	        sender.sendMessage("§4Endstone§cPvP §4§l» §c" + targetName + "'s §7IP: §c" + target.getAddress());
             
                       
                        
                
  	  	  } else {
             sender.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
              
          }
  	  	      
				return false;
        }
}