package admincmds;
 
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
 
public class warn
implements CommandExecutor, Listener
    {
       
        public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        	  if (!(sender instanceof Player)) {
                  sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
                  return true;
          }
        	  
                if (cmd.getName().equalsIgnoreCase("warn")) {
                	 if(sender.hasPermission("warn.use")){
                      
                	if (args.length < 2) {
                		
                                sender.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /warn <player> <reason>");
                                return true;
                        }      
                	}
                }
                
  	  	      if(args.length >= 1) {
  	          String targetName = args[0];
  	          Player player = (Player) sender;
  	          Player target = Bukkit.getPlayer(targetName);
  	          if (target == null) {
  	              player.sendMessage("§4Endstone§cPvP §4§l» §4" + targetName + " §cis offline!");
  	              
  	              return true;
  	          }
  	  	  
   	         if(target.hasPermission("warn.no")){
   	        	 sender.sendMessage("§4Endstone§cPvP §4§l» §cYou cannot warn that player!");
   	        	 return true;
   	         }
  	        if(args.length >= 2) {
                        String msg = "";
                        for (int i = 1; i < args.length; i++) {
                                msg += args[i] + " ";
                                
                        }
                        Bukkit.broadcast("§4Endstone§cPvP §4§l» §c" + sender.getName() + " §7warned §c" + targetName + " §7for: §c" + msg, "warn.see");
                        sender.sendMessage("§4Endstone§cPvP §4§l» §cYou §7have warned §c" + targetName + " §7for: §c" + msg);
                        target.sendMessage("§4Endstone§cPvP §4§l» §cYou §7has been warned for: §c" + msg);
                       
                        
                }
  	  	  } else {
             sender.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
              
          }
  	  	      
				return false;
        }
}