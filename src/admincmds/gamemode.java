package admincmds;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class gamemode
implements CommandExecutor
{
 

  @SuppressWarnings({ "unchecked", "rawtypes" })
public Map<String, String> oto = new HashMap();
  


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (!(sender instanceof Player)) {
		      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
		      return true;
		    }
		
		Player p = (Player)sender;
	    if (commandLabel.equalsIgnoreCase("gamemode") || commandLabel.equalsIgnoreCase("gm")) {
	      if (p.hasPermission("gamemode.use")) {
	        if (args.length == 0) {
	        	      p.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /gamemode <mode>");
			      
	        	      
	        }
            
		      
	        if ((args.length == 1) && 
	        	      (args[0].equalsIgnoreCase("survival") || args[0].equalsIgnoreCase("0") || args[0].equalsIgnoreCase("s")) && 
	        	      (p.hasPermission("gamemode.use")) && 
	        	      (!this.oto.containsKey(p.getName())))
	        	    {
	        	p.setGameMode(GameMode.SURVIVAL);
	            p.sendMessage("§4Endstone§cPvP §4§l» §7Your gamemode has been changed!");
	        	
	        
	        		    }
	        if ((args.length == 1) && 
	        	      (args[0].equalsIgnoreCase("creative") || args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("c")) && 
	        	      (p.hasPermission("gamemode.use")) && 
	        	      (!this.oto.containsKey(p.getName())))
	        	    {
	        	p.setGameMode(GameMode.CREATIVE);
	        	 p.sendMessage("§4Endstone§cPvP §4§l» §7Your gamemode has been changed!");
	        	
	        
	        		    }
	        if ((args.length == 1) && 
	        	      (args[0].equalsIgnoreCase("adventure") || args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("a")) && 
	        	      (p.hasPermission("gamemode.use")) && 
	        	      (!this.oto.containsKey(p.getName())))
	        	    {
	        	p.setGameMode(GameMode.ADVENTURE);
	        	 p.sendMessage("§4Endstone§cPvP §4§l» §7Your gamemode has been changed!");
	        	
	            
	        
	        		    }
	        if ((args.length == 1) && 
	        	      (args[0].equalsIgnoreCase("spectator") || args[0].equalsIgnoreCase("3")) && 
	        	      (p.hasPermission("gamemode.use")) && 
	        	      (!this.oto.containsKey(p.getName())))
	        	    {
	        	p.setGameMode(GameMode.SPECTATOR);
	        	 p.sendMessage("§4Endstone§cPvP §4§l» §7Your gamemode has been changed!");
	        	
	            
	        
	        		    }
                    
            	 
	      }


	      }
		return false;
  


}
		
		 		
}