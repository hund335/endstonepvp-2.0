package admincmds;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

public class enchant
implements CommandExecutor
{
 

  @SuppressWarnings({ "unchecked", "rawtypes" })
public Map<String, String> oto = new HashMap();
  


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (!(sender instanceof Player)) {
		      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
		      return true;
		    }
		
		
		Player p = (Player)sender;
	    if (commandLabel.equalsIgnoreCase("enchant")) {
	      if (p.hasPermission("enchant.use")) {
	    	  if (args.length < 2) {
	        	      p.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /enchant <enchantment> <level>");
			      
	        	      
	        }
        		    
		      
	        if ((args.length == 2) && 
	        	      (args[0].equalsIgnoreCase("sharpness") || args[0].equalsIgnoreCase("sharp")) && 
	        	      (p.hasPermission("enchant.use")) && 
	        	      (!this.oto.containsKey(p.getName())))
	        	    {
	        	int amount = Integer.parseInt(args[1]);
	        	p.sendMessage("§4Endstone§cPvP §4§l» §7You added §cSharpness " + amount + " §7to your item.");
	            p.getItemInHand().addUnsafeEnchantment(Enchantment.DAMAGE_ALL, amount);
	        	    }
	            if ((args.length == 2) && 
		        	      (args[0].equalsIgnoreCase("fireaspect") || args[0].equalsIgnoreCase("fire")) && 
		        	      (p.hasPermission("enchant.use")) && 
		        	      (!this.oto.containsKey(p.getName())))
		        	    {
	            	int amount2 = Integer.parseInt(args[1]);
		        	p.sendMessage("§4Endstone§cPvP §4§l» §7You added §cFire Aspect " + amount2 + " §7to your item.");
		            p.getItemInHand().addUnsafeEnchantment(Enchantment.FIRE_ASPECT, amount2);
		        	    }
	        		    
	            if ((args.length == 2) && 
		        	      (args[0].equalsIgnoreCase("knockback") || args[0].equalsIgnoreCase("knock")) && 
		        	      (p.hasPermission("enchant.use")) && 
		        	      (!this.oto.containsKey(p.getName())))
		        	    {
	            	int amount3 = Integer.parseInt(args[1]);
		        	p.sendMessage("§4Endstone§cPvP §4§l» §7You added §cKnockback " + amount3 + " §7to your item.");
		            p.getItemInHand().addUnsafeEnchantment(Enchantment.KNOCKBACK, amount3);
		        	    }
	        		    
	            if ((args.length == 2) && 
		        	      (args[0].equalsIgnoreCase("looting")) && 
		        	      (p.hasPermission("enchant.use")) && 
		        	      (!this.oto.containsKey(p.getName())))
		        	    {
	            	int amount4 = Integer.parseInt(args[1]);
		        	p.sendMessage("§4Endstone§cPvP §4§l» §7You added §cLooting " + amount4 + " §7to your item.");
		            p.getItemInHand().addUnsafeEnchantment(Enchantment.LOOT_BONUS_MOBS, amount4);
		        	    }
	        		    
	            if ((args.length == 2) && 
		        	      (args[0].equalsIgnoreCase("smite")) && 
		        	      (p.hasPermission("enchant.use")) && 
		        	      (!this.oto.containsKey(p.getName())))
		        	    {
	            	int amount5 = Integer.parseInt(args[1]);
		        	p.sendMessage("§4Endstone§cPvP §4§l» §7You added §cSmite " + amount5 + " §7to your item.");
		            p.getItemInHand().addUnsafeEnchantment(Enchantment.DAMAGE_UNDEAD, amount5);
		        	    }
	        		    
	            if ((args.length == 2) && 
		        	      (args[0].equalsIgnoreCase("baneofarthropds")) && 
		        	      (p.hasPermission("enchant.use")) && 
		        	      (!this.oto.containsKey(p.getName())))
		        	    {
	            	int amount6 = Integer.parseInt(args[1]);
		        	p.sendMessage("§4Endstone§cPvP §4§l» §7You added §cBane of Arthropods " + amount6 + " §7to your item.");
		            p.getItemInH