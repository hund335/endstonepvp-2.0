package admincmds;
 
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
 
public class vanish
implements CommandExecutor, Listener
    {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ArrayList<String> hiddenUsernames = new ArrayList();
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	  {
	    if (!(sender instanceof Player)) {
	      return false;
	    }
	    Player player = (Player)sender;
	    if (args.length > 0)
	    {
	      if (args[0].equalsIgnoreCase("list"))
	      {
	        if (!player.hasPermission("vanish.use"))
	        {
	          player.sendMessage("§4Endstone§cPvP §4§l» §cNope");
	          return true;
	        }
	        if (this.hiddenUsernames.size() > 0)
	        {
	          StringBuilder builder = new StringBuilder();
	          for (int i = 0; i < this.hiddenUsernames.size(); i++)
	          {
	            builder.append((String)this.hiddenUsernames.get(i));
	            if (i < this.hiddenUsernames.size() - 1) {
	              builder.append(", ");
	            }
	          }
	          player.sendMessage(ChatColor.GRAY + "Hidden players: " + builder.toString());
	        }
	        else
	        {
	          player.sendMessage("§4Endstone§cPvP §4§l» §cNon players are in vanish right now!");
	        }
	        return true;
	      }
	      if ((args[0].equalsIgnoreCase("help")) || (args[0].equalsIgnoreCase("?"))) {
	        return false;
	      }
	      if (player.hasPermission("endstoneapi.vanish.other"))
	      {
	        Player target = Bukkit.getServer().getPlayer(args[0]);
	        if (target == null) {
	          target = Bukkit.getServer().getPlayerExact(args[0]);
	        }
	        if (target == null)
	        {
	          player.sendMessage("§4Endstone§cPvP §4§l» §cUnknown player");
	          return true;
	        }
	        if (!isVanished(target))
	        {
	          vanishPlayer(target);
	          target.sendMessage("§4Endstone§cPvP §4§l» §7Enabled vanish for §c" + player.getName());
	          target.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 10000000, 1));
	          player.sendMessage("§4Endstone§cPvP §4§l» §c" + target.getName() + " §7is now vanish");
	          return true;
	        }
	        showPlayer(target);
	        target.sendMessage("§4Endstone§cPvP §4§l» §cYou are no longer invisible");
	        target.removePotionEffect(PotionEffectType.INVISIBILITY);
	        player.sendMessage("§4Endstone§cPvP §4§l» §c" + target.getName() + " §cis no longer in vanish.");
	        
	        return true;
	      }
	    }
	    else if (player.hasPermission("vanish.use"))
	    {
	      if (!isVanished(player))
	      {
	        vanishPlayer(player);
	        player.sendMessage("§4Endstone§cPvP §4§l» §7Vanish enabled for §c" + player.getName());
	        player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 10000000, 1));
	        
	        return true;
	      }
	      showPlayer(player);
	      player.sendMessage("§4Endstone§cPvP §4§l» §7Vanish disabled for §c"+ player.getName());
	      player.removePotionEffect(PotionEffectType.INVISIBILITY);
	      
	      return true;
	    }
	    return false;
	  }
	public boolean isVanished(Player player)
	  {
	    return this.hiddenUsernames.contains(player.getName());
	  }
	  
	  public void vanishPlayer(Player player)
	  {
	    this.hiddenUsernames.add(player.getName());
	    for (Player p1 : Bukkit.getServer().getOnlinePlayers()) {
	      if (p1 != player) {
	        if (p1.hasPermission("vanish.use"))
	        {
	          p1.sendMessage(ChatColor.GREEN + player.getName() + " vanished");
	        }
	        else if (p1.hasPermission("vanish.use"))
	        {
	          p1.hidePlayer(playe