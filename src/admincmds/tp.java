package admincmds;
 
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
 
public class tp
implements CommandExecutor, Listener
    {
       
        public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        	  if (!(sender instanceof Player)) {
                  sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
                  return true;
          }
        	  
                if (cmd.getName().equalsIgnoreCase("tp")) {
                	 if(sender.hasPermission("tp.use")){
                      
                	if (args.length < 1) {
                		
                                sender.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /tp <player>");
                                return true;
                        }      
                	}
                }
                
  	  	      if(args.length >= 1) {
  	          String targetName = args[0];
  	          Player player = (Player) sender;
  	          Player target = Bukkit.getPlayer(targetName);
  	          if (target == null) {
  	              player.sendMessage("§4Endstone§cPvP §4§l» §4" + targetName + " §cis offline!");
  	              
  	              return true;
  	          }
  	        if (target == player)
            {
              player.sendMessage("§4Endstone§cPvP §4§l» §cYou cannot teleport to yourself!");
              
              return false;
            }
  	        
  	  	  Bukkit.broadcast("§4Endstone§cPvP §4§l» §c" + player.getName() + " §7teleported to: §c" + targetName + "§7!", "tp.see");
  	  	      player.sendMessage("§4Endstone§cPvP §4§l» §7Teleporting to: §c" + targetName + "§7.");
  	      player.teleport(target);
                        
                       
  	  	      
                
  	  	  } else {
             sender.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
              
          }
			return false;
        }
    }
        

    