package admincmds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class weaponkit
implements CommandExecutor, Listener
    {

  @SuppressWarnings({ "unchecked", "rawtypes" })
public Map<String, String> oto = new HashMap();
public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
  
  @SuppressWarnings({ "unchecked", "rawtypes", "unused" })
public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
	  if (!(sender instanceof Player)) {
	      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
	      return true;
	    }
	  
    Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("weaponkit")) {
      if (p.hasPermission("weaponkit.use")) {
        if (args.length == 0) {
          p.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /weaponkit <kit>");
        }
      }
      else
      {
        p.sendMessage(ChatColor.GRAY + "§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
      }
    }
    else
    {
      p.sendMessage(ChatColor.GRAY + "§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("withersword")) && 
      (p.hasPermission("weaponkit.withersword"))) {
      if (this.oto.containsKey(p.getName())) {
        PlayerInventory localPlayerInventory1 = p.getInventory();
      }
      else
      {
        PlayerInventory pi = p.getInventory();
        p.sendMessage("§4Endstone§cPvP §4§l» §7You have successfully claimed your weaponkit.");
        ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);
        sword.addEnchantment(Enchantment.DAMAGE_ALL, 4);
        sword.addEnchantment(Enchantment.FIRE_ASPECT, 1);
        ItemMeta swordmeta = sword.getItemMeta();
        swordmeta.setDisplayName(ChatColor.DARK_PURPLE + "Wither Sword");
        List lore = new ArrayList();
        lore.add(ChatColor.GRAY + "Witherness I");
        lore.add(ChatColor.GOLD + "Soulbound");
        swordmeta.setLore(lore);
        sword.setItemMeta(swordmeta);
        pi.addItem(new ItemStack[] { sword });
      }

    }

    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("poisonsword")) && 
      (p.hasPermission("weaponkit.poisonsword"))) {
      if (!this.oto.containsKey(p.getName()))
      {
        PlayerInventory pi = p.getInventory();
        p.sendMessage("§4Endstone§cPvP §4§l» §7You have successfully claimed your weaponkit.");
        ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);
        sword.addEnchantment(Enchantment.DAMAGE_ALL, 4);
        sword.addEnchantment(Enchantment.FIRE_ASPECT, 1);
        ItemMeta swordmeta = sword.getItemMeta();
        swordmeta.setDisplayName(ChatColor.DARK_PURPLE + "Poison Sword");
        List lore = new ArrayList();
        lore.add(ChatColor.GRAY + "Poison Effect II");
        lore.add(ChatColor.GOLD + "Non-sharable");
        swordmeta.setLore(lore);
        sword.setItemMeta(swordmeta);
        pi.addItem(new ItemStack[] { sword });
      }
    } 
      
      if ((args.length == 1) && 
    	      (args[0].equalsIgnoreCase("witherbow")) && 
    	      (p.hasPermission("weaponkit.witherbow"))) {
    	      if (this.oto.containsKey(p.getName())) {
    	        PlayerInventory localPlayerInventory1 = p.getInventory();
    	      }
    	      else
    	      {
    	        PlayerInventory pi = p.getInventory();
    	        p.sendMessage("§4Endstone§cPvP §4§l» §7You have successful