package admincmds;
 
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
 
public class kick
implements CommandExecutor, Listener
    {
       
        public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        	 
           	 if(sender instanceof Player){
           		 
                	if (args.length < 2) {
                		
                                sender.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /kick <player> <reason>");
                                return true;
                        }      
                	
                }
                
  	  	      if(args.length >= 1) {
  	          String targetName = args[0];
  	          //Player player = (Player) sender;
  	          Player target = Bukkit.getPlayer(targetName);
  	          if (target == null) {
  	              sender.sendMessage("§4Endstone§cPvP §4§l» §4" + targetName + " §cis offline!");
  	              
  	              return true;
  	          }
  	         if(target.hasPermission("kick.no")){
  	        	 sender.sendMessage("§4Endstone§cPvP §4§l» §cYou cannot kick that player!");
  	        	 return true;
  	         }
  	        if(args.length >= 2) {
                        String msg = "";
                        for (int i = 1; i < args.length; i++) {
                                msg += args[i] + " ";
                                
                        }
                        Bukkit.broadcast("§4Endstone§cPvP §4§l» §c" + sender.getName() + " §7kicked §c" + targetName + " §7for: §c" + msg, "kick.see");
                        sender.sendMessage("§4Endstone§cPvP §4§l» §cYou §7have kicked §c" + targetName + " §7for: §c" + msg);
                        target.kickPlayer("§cYou §7has been kicked for: §c" + msg);
                        
                       
  	      
           }
                        	
                        
                        
                }
  	      if(!(sender instanceof Player)){
  	    	if (args.length < 2) {
        		
                sender.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /kick <player> <reason>");
                return true;
        }      
  	    	if(args.length >= 1) {
    	          String targetName2 = args[0];
    	          //Player player2 = (Player) sender;
    	          Player target2 = Bukkit.getPlayer(targetName2);
    	          if (target2 == null) {
    	              sender.sendMessage("§4Endstone§cPvP §4§l» §4" + targetName2 + " §cis offline!");
    	              
    	              return true;
    	          }
    	        if(args.length >= 2) {
                          String msg2 = "";
                          for (int i2 = 1; i2 < args.length; i2++) {
                                  msg2 += args[i2] + " ";
                                  target2.kickPlayer("§cYou §7has been kicked for: §c" + msg2);
                          }
                          
    	        }
  	    	  
  	      }
  	  	  
  	  	      
		
            
			
        }
  	      return true;
        }
    }
    