package admincmds;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import configs.warps;

@SuppressWarnings("unused")
public class setwarp
  implements CommandExecutor
{
	 warps settings = warps.getInstance();
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
    	sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
      return true;
    }
    Player p = (Player)sender;
    if (cmd.getName().equalsIgnoreCase("setwarp"))
    {
      if (args.length == 0)
      {
        p.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /setwarp <name>");
        return true;
      }
      this.settings.getData().set("warps." + args[0] + ".world", p.getLocation().getWorld().getName());
      this.settings.getData().set("warps." + args[0] + ".x", Double.valueOf(p.getLocation().getX()));
      this.settings.getData().set("warps." + args[0] + ".y", Double.valueOf(p.getLocation().getY()));
      this.settings.getData().set("warps." + args[0] + ".z", Double.valueOf(p.getLocation().getZ()));
      this.settings.saveData();
      p.sendMessage("§4Endstone§cPvP §4§l» §7Warp: §c" + args[0] + " §7has been set!");
    }
    return false;
  }
}
