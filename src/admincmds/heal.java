package admincmds;
 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

 
@SuppressWarnings("unused")
public class heal
implements CommandExecutor
    {
      String prefix = ChatColor.GRAY + "[" + ChatColor.DARK_GRAY + "EndstonePvP" + ChatColor.GRAY + "]";
      
      
      public final Logger logger = Logger.getLogger("Minecraft");
 
      @SuppressWarnings({ "rawtypes", "unchecked" })
    public Map<String, String> oto = new HashMap();
      public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
     
       
        public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
                return true;
        }
           final Player p = (Player)sender;
            if (commandLabel.equalsIgnoreCase("heal")){
                if(p.hasPermission("heal.use")){
                    if (args.length == 0) {
                    	int cooldownTime1 = 10; // Get number of seconds from wherever you want
                        if(cooldowns.containsKey(sender.getName())) {
                            long secondsLeft = ((cooldowns.get(sender.getName())/60000)+cooldownTime1) - (System.currentTimeMillis()/60000);
                            if(secondsLeft>0) {
                                // Still cooling down
                                p.sendMessage("§4Endstone§cPvP §4§l» §cYou need to wait §4§o" + secondsLeft + " minutes§c.");
                                return true;
                            }
                        }
                        // No cooldown found or cooldown has expired, save new cooldown
                        cooldowns.put(sender.getName(), System.currentTimeMillis());
                        Bukkit.broadcastMessage("§4Endstone§cPvP §4§l» §c" + sender.getName() + " §7used the heal ability!");
                        p.sendMessage("§4Endstone§cPvP §4§l» §7You have been healed.");
                        p.setHealth(20);
                        p.setFoodLevel(20);
                        p.setFireTicks(0);
                        p.removePotionEffect(PotionEffectType.BLINDNESS);
                        p.removePotionEffect(PotionEffectType.CONFUSION);
                        p.removePotionEffect(PotionEffectType.SLOW);
                        p.removePotionEffect(PotionEffectType.HUNGER);
                        p.removePotionEffect(PotionEffectType.POISON);
                        p.removePotionEffect(PotionEffectType.WEAKNESS);
                        p.removePotionEffect(PotionEffectType.WITHER);
                        
                        
                        }
                    
                  
                } else {
                    p.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
                    
                }
        
            }
			return false;
        }
    }
                
 
 