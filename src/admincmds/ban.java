package admincmds;
 
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
 
@SuppressWarnings("unused")
public class ban
implements CommandExecutor, Listener
    {
       
        @SuppressWarnings("deprecation")
		public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        	 
           	 if(sender instanceof Player){
           		 
                	if (args.length < 2) {
                		
                                sender.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /ban <player> <reason>");
                                return true;
                        }      
                	
                }
                
  	  	      if(args.length >= 1) {
  	          String targetName = args[0];
  	          //Player player = (Player) sender;
  	          Player target = Bukkit.getPlayer(targetName);
  	          if (target == null) {
  	              sender.sendMessage("§4Endstone§cPvP §4§l» §4" + targetName + " §cis offline!");
  	              
  	              return true;
  	          }
  	  	  
   	         if(target.hasPermission("ban.no")){
   	        	 sender.sendMessage("§4Endstone§cPvP §4§l» §cYou cannot ban that player!");
   	        	 return true;
   	         }
  	        if(args.length >= 2) {
                        String msg = "";
                        for (int i = 1; i < args.length; i++) {
                                msg += args[i] + " ";
                                
                        }
                        Bukkit.broadcast("§4Endstone§cPvP §4§l» §c" + sender.getName() + " §7banned §c" + targetName + " §7for: §c" + msg, "ban.see");
                        sender.sendMessage("§4Endstone§cPvP §4§l» §cYou §7have banned §c" + targetName + " §7for: §c" + msg);
                        target.kickPlayer("§cYou §7has been banned for: §c" + msg);
                        target.setBanned(true);
                        
                        
                       
  	      
           }
                        	
                        
                        
                }
  	      if(!(sender instanceof Player)){
  	    	if (args.length < 2) {
        		
                sender.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /ban <player> <reason>");
                return true;
        }      
  	    	if(args.length >= 1) {
    	          String targetName2 = args[0];
    	          //Player player2 = (Player) sender;
    	          Player target2 = Bukkit.getPlayer(targetName2);
    	          if (target2 == null) {
    	              sender.sendMessage("§4Endstone§cPvP §4§l» §4" + targetName2 + " §cis offline!");
    	              
    	              return true;
    	          }
    	        if(args.length >= 2) {
                          String msg2 = "";
                          for (int i2 = 1; i2 < args.length; i2++) {
                                  msg2 += args[i2] + " ";
                                  target2.kickPlayer("§cYou §7has been kicked for: §c" + msg2);
                                  target2.setBanned(true);
                          }

  	      
  	      }
  	    	}
  	      }
   
  	      return true;
        }
        @EventHandler//(priority= EventPriority.NORMAL)
        public void playerBanLoginEvent(PlayerLoginEvent e) {
            Result r = e.getResult();
           
            if(r == Result.KICK_BANNED) {
                e.setKickMessage("§cBan Type: §4Perm\n§cWant Unban?: §4http://endstonepvp.enjin.com");
            }
        }
    }
    