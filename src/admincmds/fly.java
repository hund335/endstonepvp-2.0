package admincmds;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class fly
implements CommandExecutor
{
 

  @SuppressWarnings({ "unchecked", "rawtypes" })
public Map<String, String> oto = new HashMap();
  


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (!(sender instanceof Player)) {
		      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
		      return true;
		    }
		
		Player p = (Player)sender;
	    if (commandLabel.equalsIgnoreCase("fly")) {
	      if (p.hasPermission("fly.use")) {
	        if (args.length == 0) {
	        	      p.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /fly <on:off>");
			      
	        	      
	        }
            
		      
	        if ((args.length == 1) && 
	        	      (args[0].equalsIgnoreCase("on")) && 
	        	      (p.hasPermission("fly.use")) && 
	        	      (!this.oto.containsKey(p.getName())))
	        	    {
	        	
	            p.sendMessage("§4Endstone§cPvP §4§l» §7You have turned fly on!");
	            p.setAllowFlight(true);
	        
	        		    }
	        if ((args.length == 1) && 
	        	      (args[0].equalsIgnoreCase("off")) && 
	        	      (p.hasPermission("fly.use")) && 
	        	      (!this.oto.containsKey(p.getName())))
	        	    {
	        	p.sendMessage("§4Endstone§cPvP §4§l» §7You have turned fly off!");
	        	p.setAllowFlight(false);
	        
	        	
	        
	        		    }
                    
            	 
	      }


	      }
		return false;
  


}
		
		 		
}