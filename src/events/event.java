package events;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class event
implements CommandExecutor
{
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, String> oto = new HashMap();
	
		

		  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
			  if (!(sender instanceof Player)) {
			      sender.sendMessage("§4Endstone§cPvP §4§l» §cCan only be used in-game");
			      return true;
			    }  
			  
			  
			  Player p = (Player)sender;
			    if (commandLabel.equalsIgnoreCase("event")) {
			      if (p.hasPermission("event.use")) {
			        if (args.length == 0) {
			          p.sendMessage("§4Endstone§cPvP §4§l» §cUsage: /event <event>");
			        }

			      }
			      else
			      {
			        sender.sendMessage("§4Endstone§cPvP §4§l» §cYou are not allowed to use that command.");
			      }

			    }

			    if ((args.length == 1) && 
			      (args[0].equalsIgnoreCase("sponge")) && 
			      (p.hasPermission("event.sponge")) && 
			      (!this.oto.containsKey(p.getName())))
			    {
			    	PlayerInventory pi = p.getInventory();    
	     	         ItemStack card = new ItemStack(Material.SPONGE,1);
	     	         ItemMeta cardmeta = card.getItemMeta();
	     	         cardmeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "SPONGE");
	     	         card.setItemMeta(cardmeta);
	     	         pi.addItem(card);
	
	
	       p.updateInventory();
	
	
			    }
				return false;
		  }

}