package listeners;

import java.util.Iterator;
import org.bukkit.Effect;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class listeners2
  implements Listener
{
  @SuppressWarnings({ "rawtypes" })
@EventHandler(priority=EventPriority.MONITOR)
  public void onDamage(EntityDamageByEntityEvent event)
  {
    if ((event.getDamager() instanceof Arrow)) {
      if (event.isCancelled()) return;
      Arrow arrow = (Arrow)event.getDamager();
      Player damager = (Player)arrow.getShooter();
      LivingEntity damaged = (LivingEntity)event.getEntity();
      if (damager.getItemInHand().hasItemMeta())
      {
        Iterator localIterator = damager.getItemInHand().getItemMeta().getLore().iterator();
        while (localIterator.hasNext()) {
          String lore = (String)localIterator.next();
          if (lore.contains("Wither")) {
            damaged.getWorld().playEffect(damaged.getLocation(), Effect.POTION_BREAK, 8);
          }
          else if (lore.contains("Poison Shoot")) {
            damaged.getWorld().playEffect(damaged.getLocation(), Effect.POTION_BREAK, 4);
          }
          else if (lore.contains("Blind Shoot")) {
            damaged.getWorld().playEffect(damaged.getLocation(), Effect.POTION_BREAK, 14);
          }
          else if (lore.contains("Slow Shoot")) {
            damaged.getWorld().playEffect(damaged.getLocation(), Effect.POTION_BREAK, 10);
          }
          else if (lore.contains("Weak Shoot")) {
            damaged.getWorld().playEffect(damaged.getLocation(), Effect.POTION_BREAK, 14);
          }
          else if (lore.contains("Confusion Shoot")) {
          damaged.getWorld().playEffect(damaged.getLocation(), Effect.POTION_BREAK, 4);
          }
          if (lore.contains("Wither Shoot III")) {
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 120, 2));
          }
          else if (lore.contains("Wither Shoot II")) {
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 100, 1));
          }
          else if (lore.contains("Wither Shoot I")) {
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 80, 0));
          }
          else if (lore.contains("Poison Shoot III")) {
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 120, 2));
          }
          else if (lore.contains("Poison Shoot II")) {
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 100, 1));
          }
          else if (lore.contains("Poison Shoot I")) {
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 80, 0));
          }
          else if (lore.contains("Blind Shoot III")) {
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 120, 2));
          }
          else if (lore.contains("Blind Shoot II")) {
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 1));
          }
          else if (lore.contains("Blind Shoot I")) {
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 80, 0));
          }
          else if (lore.contains("Slow Shoot III")) {
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 120, 2));
          }
          else if (lore.contains("Slow Shoot II")) {
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 100, 1));
          }
          else if (lore.contains("Slow Shoot I")) {
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 80, 0));
          }
          else if (lore.contains("Weak Shoot III")) {
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 120, 2));
          }
          else if (lore.contains("Weak Shoot II")) {
            damag