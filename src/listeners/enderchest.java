package listeners;

import java.util.ArrayList;
import java.util.List;

import mc.hund35.main.main;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

@SuppressWarnings("unused")
public class enderchest
  implements Listener
{
  private final main plugin;
  @SuppressWarnings({ "unchecked", "rawtypes" })
private List<String> eventRunning = new ArrayList();

  public enderchest(main plugin)
  {
    this.plugin = plugin;
  }

  @EventHandler
  public void onInventoryClose(InventoryCloseEvent event)
  {
    Player player = (Player)event.getPlayer();
    if (this.plugin.invFile.isEditing(player.getName()))
    {
      this.plugin.invFile.saveInventory(this.plugin.invFile.getEditTarget(player.getName()), event.getView().getTopInventory());
      this.plugin.invFile.stopEditing(player.getName());
      return;
    }
    if (!this.plugin.invFile.hasInventoryOpened(player.getName())) {
      return;
    }
    this.plugin.invFile.saveInventory(player.getName(), event.getView().getTopInventory());
    this.plugin.invFile.setInventoryOpened(player.getName(), Boolean.valueOf(false));
    player.playSound(player.getLocation(), Sound.BLOCK_CHEST_CLOSE, 1.0F, 1.0F);
  }

  @EventHandler
  public void onPlayerQuit(PlayerQuitEvent event)
  {
    Player player = event.getPlayer();
    if (this.plugin.invFile.hasInventoryOpened(player.getName()))
      player.closeInventory();
  
  }
}

  /*
  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent event)
  {
    event(event);
  }

  public boolean stop(Player player)
  {
    this.eventRunning.remove(player.getName());
    return false;
  }

  public boolean event(PlayerInteractEvent event)
  {
    Player player = event.getPlayer();
    if (this.eventRunning.contains(player.getName()))
    {
      this.plugin.getLogger().info("duplicate event for " + player);
      return stop(player);
    }
    this.eventRunning.add(player.getName());
    if ((player.getWorld() != Bukkit.getWorld("world")) && 
      (player.getWorld() != Bukkit.getWorld("faction")) && 
      (player.getWorld() != Bukkit.getWorld("spawn"))) {
      return stop(player);
    }
    if (event.getClickedBlock() == null) {
      return stop(player);
    }
    if (!event.getClickedBlock().getType().equals(Material.ENDER_CHEST)) {
      return stop(player);
    }
    if (player.getGameMode() == GameMode.CREATIVE) {
      return stop(player);
    }
    event.setCancelled(true);
    if (!player.hasPermission("enderchest.use"))
    {
	
      player.sendMessage("§4Endstone§cPvP §4§l» §cYou cant open the enderchest.");
      return stop(player);
    }
    if (this.plugin.invFile.isBeingEdited(player.getName()))
    {
      player.sendMessage("§7Your enderchest is currently being edited by");
      player.sendMessage("§7can administrator, try opening it again later.");
      return stop(player);
    }
    if (this.plugin.invFile.hasInventoryOpened(player.getName())) {
      return stop(player);
    }
    player.openInventory(this.plugin.invFile.getInventory(player.getName()));
    this.plugin.invFile.setInventoryOpened(player.getName(), Boolean.valueOf(true));
    player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1.0F, 1.0F);
    return stop(player);
  }
}
 */
  
