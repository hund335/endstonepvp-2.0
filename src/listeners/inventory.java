package listeners;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mc.hund35.main.main;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class inventory
{
  private final main plugin;
  @SuppressWarnings({ "unchecked", "rawtypes" })
private HashMap<String, String> edit = new HashMap();
  @SuppressWarnings({ "unchecked", "rawtypes" })
private List<String> chest = new ArrayList();

  public inventory(main plugin)
  {
    this.plugin = plugin;
  }

  public void checkFolders()
  {
    File invFolder = new File(this.plugin.getDataFolder() + File.separator + "inventories");
    if (!invFolder.exists())
      invFolder.mkdirs();
  }

  public void closeAllInventories()
  {
    for (String loop : this.chest)
    {
      Player target = Bukkit.getPlayer(loop);
      if (target != null)
        target.closeInventory();
    }
  }

  public void setInventoryOpened(String player, Boolean opened)
  {
    player = player.toLowerCase();
    if (opened.booleanValue())
    {
      if (!this.chest.contains(player)) {
        this.chest.add(player);
      }
      return;
    }
    if (this.chest.contains(player))
      this.chest.remove(player);
  }

  public boolean hasInventoryOpened(String player)
  {
    player = player.toLowerCase();
    return this.chest.contains(player);
  }

  public boolean isBeingEdited(String target)
  {
    target = target.toLowerCase();
    return this.edit.containsValue(target);
  }

  public boolean isEditing(String player)
  {
    player = player.toLowerCase();
    return this.edit.containsKey(player);
  }

  public void stopEditing(String player)
  {
    player = player.toLowerCase();
    if (isEditing(player))
      this.edit.remove(player);
  }

  public String getEditTarget(String player)
  {
    player = player.toLowerCase();
    if (!isEditing(player)) {
      return null;
    }
    return (String)this.edit.get(player);
  }

  public void editInventory(Player player, String target)
  {
    target = target.toLowerCase();
    if (!isEditing(player.getName().toLowerCase()))
    {
      player.openInventory(getInventory(target));
      this.edit.put(player.getName().toLowerCase(), target);
    }
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
public ItemStack setTitle(ItemStack is, String title)
  {
    ItemMeta meta = is.getItemMeta();
    meta.setDisplayName((title));
    List lore = new ArrayList();
    lore.add(("&cPlease don't put items here"));
    meta.setLore(lore);
    is.setItemMeta(meta);
    return is;
  }

  public void saveInventoryItem(String player, Integer slot, ItemStack item)
  {
    try
    {
      player = player.toLowerCase();
      File invFile = new File(this.plugin.getDataFolder() + File.separator + "inventories" + File.separator + player + ".inv");
      if (!invFile.exists()) {
        invFile.createNewFile();
      }
      YamlConfiguration config = YamlConfiguration.loadConfiguration(invFile);
      config.set("inventory." + slot, item);
      config.save(invFile);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  public void saveInventory(String player, Inventory inv)
  {
    try
    {
      player = player.toLowerCase();
      Integer slots = Integer.valueOf(inv.getSize());
      File invFile = new File(this.plugin.getDataFolder() + File.separator + "inventories" + File.separator + player + ".inv");
      if (!invFile.exists()) {
        invFile.createNewFile();
      }
      YamlConfiguration config = YamlConfiguration.loadConfiguration(invFile);
      for (int i = 0; i < slots.intValue(); i++) {
        config.set("inventory." + i, inv.getItem(i));
      }
      config.save(invFile);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  public Inventory getInventory(String player)
  {
    player = player.toLowerCase();
    Integer slots = Integer.valueOf(54);
    Invent