package listeners;

import mc.hund35.main.main;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import admincmds.vanish;

@SuppressWarnings("unused")
public class vanishlogin
  implements Listener
{
  private vanish plugin;
  
  public vanishlogin(vanish plugin)
  {
    this.plugin = plugin;
  }
  
  @EventHandler
  public void handleLogin(PlayerLoginEvent event)
  {
    Player player = event.getPlayer();
    if (this.plugin.isVanished(player)) {
      this.plugin.showPlayer(player);
    }
    if (player.hasPermission("endstoneapi.vanish.seeall")) {
      return;
    }
    for (Player p1 : Bukkit.getServer().getOnlinePlayers()) {
      if ((this.plugin.isVanished(p1)) && (player != p1) && (p1 != null)) {
        player.hidePlayer(p1);
      }
    }
  }
  
  @EventHandler
  public void handleQuit(PlayerQuitEvent event)
  {
    Player player = event.getPlayer();
    if (this.plugin.isVanished(player)) {
      this.plugin.showPlayer(player);
    }
  }
}